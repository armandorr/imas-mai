package eu.su.mas.dedaleEtu.mas.knowledge;

import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Defines the attributes of a found treasure.
 * @param nodeId
 * @param treasureType (diamond,gold)
 * @param amount, look_picking, strength, is_open
 */
public class Treasures {
    private final ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> listOfTreasures = new ArrayList<>();

    public Treasures() {
    }

    public void addIfTreasure(String nodeId, String treasureType, Tuple4<Integer, Integer, Integer, Boolean> treasuresValues){
        boolean found = false;
        for (Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure: listOfTreasures){
            if(Objects.equals(nodeId, treasure.getFirst())) {
                found = true;
                break;
            }
        }
        if (!found) this.addTreasure(nodeId, treasureType, treasuresValues);
    }
    public void addTreasure(String nodeId, String treasureType, Tuple4<Integer, Integer, Integer, Boolean> treasuresValues) {
        Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure = new Tuple3<>(nodeId, treasureType, treasuresValues);
        listOfTreasures.add(treasure);
    }

    public ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getTreasures() {
        return listOfTreasures;
    }

    public ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getClosedTreasures() {
        ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> returnList = new ArrayList<>();
        for (Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> a : listOfTreasures){
            if (!a.getThird().get_4()) returnList.add(a);
        }
        return returnList;
    }

    public synchronized void setIthTreasure(int index, Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure)
    {
        this.listOfTreasures.set(index, treasure);
    }

    public synchronized void setNodeTreasure(String nodeId, Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> newTreasure){
        for (int i = 0; i < this.listOfTreasures.size(); i++) {
            Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> oldTreasure = this.listOfTreasures.get(i);
            if (Objects.equals(oldTreasure.getFirst(), nodeId)) {
                setIthTreasure(i,newTreasure);
                break;
            }
        }
    }
}
