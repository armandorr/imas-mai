package eu.su.mas.dedaleEtu.mas.behaviours;
import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;

import eu.su.mas.dedaleEtu.mas.agents.FacilitatorAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
NOT FINISHED
 */
public class ReceiveOrderBehaviour extends OneShotBehaviour {

    public ReceiveOrderBehaviour(Agent a)
    {
        super(a);
    }
    int exit = 3;
    @Override
    public void action() {
        exit = 3;
        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
        msg_route.setProtocol("FACILITATOR-HELLO");
        msg_route.setSender(this.myAgent.getAID());
        msg_route.addReceiver(new AID("coordinator", AID.ISLOCALNAME));
        String type = Objects.equals(this.myAgent.getLocalName(), "facilitatorGold") ? "Gold" : "Diamond";
        Couple<String, String> info = new Couple<>(type, ((AbstractDedaleAgent) this.myAgent).getCurrentPosition());
        try {
            msg_route.setContentObject(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((AbstractDedaleAgent)this.myAgent).sendMessage(msg_route);

        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MessageTemplate msgTemplate = MessageTemplate.and(
                MessageTemplate.MatchProtocol("ROUTE-FACILITATOR"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM));

        List<ACLMessage> msgRouteList = this.myAgent.receive(msgTemplate,1000);


        if (msgRouteList != null) {
            ACLMessage messageInfo = msgRouteList.get(0);
            Tuple4<ArrayList<String>,ArrayList<String>,ArrayList<String>,Tuple3<String, ArrayList<String>,Integer>> infoReceived = null;
            try {
                infoReceived = (Tuple4<ArrayList<String>,ArrayList<String>,ArrayList<String>,Tuple3<String, ArrayList<String>,Integer>>) messageInfo.getContentObject();
            } catch (UnreadableException e) {
                e.printStackTrace();
            }
            if (infoReceived == null) {
                System.out.println("Im "+this.myAgent.getLocalName()+" and I'm useless, I'm gonna kill myself!");
                this.myAgent.doDelete();
                return;
            }
            ArrayList<String> inverseRoute = new ArrayList<>(infoReceived.get_1());
            Collections.reverse(inverseRoute);
            if (!inverseRoute.isEmpty()) inverseRoute.remove(0);
            inverseRoute.add(((AbstractDedaleAgent) this.myAgent).getCurrentPosition());
            ((FacilitatorAgent) this.myAgent).setInverseRoute(inverseRoute);
            ((FacilitatorAgent) this.myAgent).setMyRoute(infoReceived.get_1());
            Tuple4<ArrayList<String>, ArrayList<String>,ArrayList<String>,Integer> myAction = new Tuple4<>(infoReceived.get_2(), infoReceived.get_3(),infoReceived.get_4().getSecond(),infoReceived.get_4().getThird());
            ((FacilitatorAgent) this.myAgent).setMyAction(myAction);
            ((FacilitatorAgent) this.myAgent).setCollectorName(infoReceived.get_4().getFirst());
            try {
                this.myAgent.doWait(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.myAgent.receive(msgTemplate, 1000);
        }
    }

    public int onEnd()
    {
        return exit;
    }
}
