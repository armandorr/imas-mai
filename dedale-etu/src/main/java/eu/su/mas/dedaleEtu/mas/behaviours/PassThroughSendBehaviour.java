package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.agents.CoordinatorAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;
import eu.su.mas.dedaleEtu.mas.agents.FacilitatorAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class PassThroughSendBehaviour extends OneShotBehaviour {
    /**
     *
     */
    private final String[] receivers = {"explorer1","explorer2","explorer3","collectorGold1","collectorGold2",
                                        "collectorDiamond1","collectorDiamond2","tanker1","tanker2","coordinator","facilitatorGold","facilitatorDiamond"};

    private String typeOfAgent = "";
    private final FSMBehaviour fsm;

    /**
     * @param a the agent
     * @param typeOfAgent the type of the agent {Explorer or Collector}
     */
    public PassThroughSendBehaviour(FSMBehaviour fsm, Agent a, String typeOfAgent)
    {
        super(a);
        this.typeOfAgent = typeOfAgent;
        this.fsm = fsm;
    }

    @Override
    public void action() {

        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Objects.equals(this.typeOfAgent, "Coordinator")) {
            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("PASS-THROUGH-RANDOM");
            msg_route.setSender(this.myAgent.getAID());
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
                    msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                }
            }
            ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
        }

        if (Objects.equals(this.typeOfAgent, "Explorer") && (((ExplorerAgent)this.myAgent).getStuck() > 5)) {
            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("PASS-THROUGH-RANDOM");
            msg_route.setSender(this.myAgent.getAID());
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
                    msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                }
            }
            ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
        }
        else if (Objects.equals(this.typeOfAgent, "Collector") && (((CollectorAgent)this.myAgent).getStuck() > 5)) {
            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("PASS-THROUGH-RANDOM");
            msg_route.setSender(this.myAgent.getAID());
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
                    msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                }
            }
            ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
        }
        else if (Objects.equals(this.typeOfAgent, "Fac") && (((FacilitatorAgent)this.myAgent).getStuck() > 5)) {
            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("PASS-THROUGH-RANDOM");
            msg_route.setSender(this.myAgent.getAID());
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
                    msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                }
            }
            ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
        }
        else {
            // Send route
            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("PASS-THROUGH");
            msg_route.setSender(this.myAgent.getAID());
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
                    msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                }
            }

            ArrayList<String> route;
            if (Objects.equals(this.typeOfAgent, "Explorer")) {
                if (((ExplorerAgent) this.myAgent).getWait() || ((ExplorerAgent) this.myAgent).getMyAction() != null) return;
                route = ((ExplorerAgent) this.myAgent).getMyRoute();
            } else if (Objects.equals(this.typeOfAgent, "Collector")) { // Collector
                if (((CollectorAgent) this.myAgent).getMyAction() == null) return;//????????????
                route = ((CollectorAgent) this.myAgent).getMyRoute();
            } else if (Objects.equals(this.typeOfAgent, "Fac")) {
                if (((FacilitatorAgent) this.myAgent).getExplorationPhase()) return;
                route = ((FacilitatorAgent) this.myAgent).getMyRoute();
            }
            else{
                //if (Objects.equals(this.typeOfAgent, "Coord"))
                route = ((CoordinatorAgent) this.myAgent).getMyRoute();
            }

            String position = ((AbstractDedaleAgent) this.myAgent).getCurrentPosition();

            Couple<ArrayList<String>, String> a = new Couple<>(route, position);
            try {
                msg_route.setContentObject(a);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
        }
    }

    @Override
    public int onEnd()
    {
        // Finish with the same exit value as entered to know where to return in the next state
        return this.fsm.getLastExitValue();
    }
}
