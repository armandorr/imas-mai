package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareInfoBehaviour;


import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

public class MoveToKnownPositionBehaviour extends SimpleBehaviour {

    private static final long serialVersionUID = 8567683731496787661L;
    private boolean finished = false;
    private MapRepresentation myMap;
    private int exit;

    public MoveToKnownPositionBehaviour(final AbstractDedaleAgent myagent) {
        super(myagent);
    }

    @Override
    public void action() {
        this.myMap=((ExplorerAgent) this.myAgent).getMyMap();

        exit = 1;
        //0) Retrieve the current position
        String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
        String endPosition = ((ExplorerAgent) this.myAgent).getStartPosition();
        String treasurePosition = ((ExplorerAgent) this.myAgent).getTreasureToOpenPosition();

        if (treasurePosition == null) {
            System.out.println("Im "+this.myAgent.getLocalName()+" and im useless, im gonna kill myself!");
            this.myAgent.doDelete();
            return;
        }

        endPosition = treasurePosition;
        ArrayList<String> route = new ArrayList<>(this.myMap.getShortestPathWithoutNode(myPosition, endPosition, this.myMap.getCoordinatorPosition()));

        if (!route.isEmpty()) route.remove(route.size() - 1);
        ((ExplorerAgent) this.myAgent).setMyRoute(route);

        finished = true;
    }

    public int onEnd() {
        return exit;
    }

    @Override
    public boolean done() {
        return finished;
    }

}
