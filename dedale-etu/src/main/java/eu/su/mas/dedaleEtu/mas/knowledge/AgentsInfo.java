package eu.su.mas.dedaleEtu.mas.knowledge;

import java.util.HashMap;

import java.util.ArrayList;
import dataStructures.tuple.Tuple4;

/**
 * Defines the attributes of an agent that have been found
 */
public class AgentsInfo {
//    private final ArrayList<Tuple4<String, String, Integer, Integer>> listOfAgents = new ArrayList<Tuple4<String, String, Integer, Integer>>();
    private final HashMap<String, HashMap<String, String>> listOfAgents = new HashMap<String, HashMap<String, String>>();

    public AgentsInfo() {
    }

    public void addAgent(String agentName, HashMap<String, String> Info) {
        listOfAgents.put(agentName, Info);
    }

    public HashMap<String, HashMap<String, String>> getAgentsFound() {
        return listOfAgents;
    }

    public boolean inAgentsInfo(String nameAgent) {
        return listOfAgents.containsKey(nameAgent);
    }

    public String getAgentPosition(String name) {
        HashMap<String, String> agentinfo = listOfAgents.get(name);
        return agentinfo.get("Position");
    }
}
