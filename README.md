# IMAS-MAI Project

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <ul>
        <li><a href="#execution">Execution</a></li>
        <li><a href="#example">Example</a></li>
     </ul>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project
This project implements a Multi-Agents Systems (MAS) solution for a treasure hunt problem, where multiple treasures of different types are distributed trough the map. The main objective is to collect all the treasures with the minimum number of steps possible. To do so, the system implements different types of agentes with specific capabilities.

<p align="center"><img src="map1_distribution.png" alt="drawing" width=50% height=50%/></p>

The problem base architecture is borrowed from the [Dedale](https://github.com/BassemYagoub/Projet_Dedale_Fosyma) project, which provides a practical application of a Multi-Agents Systems. All the implementation is done under [JADE](https://jade.tilab.com/) (Java Agent DEvelopment Framework) platform and [Maven](https://maven.apache.org/).

The main agents avaible for the problem solution implementation are:
* Explorer: can generate a map and lock-pick (open) the treasures.
* Colllector: can help to lock-pick the trasures and gather the goods from the treasure.
* Tanker: can save the goods collected from the treasures.

The extra agents added to coordinate and help with the solution are:
* Coordinator: can merge the maps from the explorers and generate orders for the collectors and explorers.
* Facilitator: transmits the messages between the coordinator and the collectors.

To make the problem more interesting, agents have limited capabilites of trasure opening, thus cooperations need to be carried to open the treasures with multiple agents at one, also only the agents witht the same type as the treasue can gather the goods with a limited amount, and the tankes can only carry goods of their type.

The architecture of the described Multi-Agents Systems is the following:

<p align="center"><img src="agentsDiagrams-architecture.png" alt="drawing" width=80% height=80%/></p>
<p align="right">(<a href="#readme">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started
To get a local copy up and running follow these simple steps.

### Prequisites
To run the software you will need:
* [IntelliJ IDEA](https://www.jetbrains.com/idea/download/)
* [Maven](https://maven.apache.org/download.cgi)

### Installation
1. Clone the repo

   ```git clone https://github.com/arodriguez99/IMAS-MAI.git ```

<p align="right">(<a href="#readme">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
## Usage

### Execution
To use the code, source the code folder with:

```cd dedale-etu``` 

To execute the problem simulation use:

``` mvn install exec:java``` 

### Example
Speed up example of a 6min succesfull simulation.

[ExampleVideo](https://user-images.githubusercontent.com/47107019/214125943-8290ccbd-0ad6-4301-9b53-278b1089c9d8.mp4)

<p align="right">(<a href="#readme">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Alvaro Armada - alvaro.armada@estudiantat.upc.edu

Izan Leal - izan.upc@gmail.com

Armando Rodriguez Ramos - armando.rodriguez@estudiantat.upc.edu

Hasnain Shafqat - hussnain.shafqat@estudiantat.upc.edu


<p align="right">(<a href="#readme">back to top</a>)</p>

