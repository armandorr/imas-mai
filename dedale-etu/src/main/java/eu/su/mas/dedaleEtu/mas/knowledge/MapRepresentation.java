package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.stream.Stream;

import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import org.graphstream.algorithm.Dijkstra;
import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.ElementNotFoundException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.IdAlreadyInUseException;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.fx_viewer.FxViewer;
import org.graphstream.ui.view.Viewer;

import dataStructures.serializableGraph.*;
import dataStructures.tuple.Couple;
import javafx.application.Platform;

/**
 * This simple topology representation only deals with the graph, not its content.</br>
 * The knowledge representation is not well written (at all), it is just given as a minimal example.</br>
 * The viewer methods are not independent of the data structure, and the dijkstra is recomputed every-time.
 * 
 * @author hc
 */
public class MapRepresentation implements Serializable {

	public enum MapAttribute {
		agent,open,closed;
	}

	private static final long serialVersionUID = -1333959882640838272L;

	/*********************************
	 * Parameters for graph rendering
	 ********************************/

	private String defaultNodeStyle= "node {"+"fill-color: black;"+" size-mode:fit;}";
	private String nodeStyle_open = "node.agent {"+"fill-color: forestgreen;"+"}";
	private String nodeStyle_agent = "node.open {"+"fill-color: blue;"+"}";
	//private String nodeStyle_treasure = "node.open {"+"fill-color: yellow;"+"}";
	private String nodeStyle=defaultNodeStyle+nodeStyle_agent+nodeStyle_open;

	private Graph g; //data structure non serializable
	private Viewer viewer; //ref to the display,  non serializable
	private Integer nbEdges;//used to generate the edges ids

	private SerializableSimpleGraph<String, MapAttribute> sg;//used as a temporary dataStructure during migration

	private final Treasures allTreasures = new Treasures();
	private final Treasures goldTreasures = new Treasures();
	private final Treasures diamondTreasures = new Treasures();
	private final AgentsInfo agentsinfo = new AgentsInfo();


	public MapRepresentation() {
		//System.setProperty("org.graphstream.ui.renderer","org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		System.setProperty("org.graphstream.ui", "javafx");
		this.g= new SingleGraph("My world vision");
		this.g.setAttribute("ui.stylesheet",nodeStyle);

		/*Platform.runLater(() -> {
			openGui();
		});*/
		//this.viewer = this.g.display();

		this.nbEdges=0;
	}

	public synchronized void addIfTreasures(String nodeId, ArrayList<Couple<Observation, Integer>> listOfTreasures){
		HashMap<String, Integer> dictionary = new HashMap<>();
		for (Couple<Observation, Integer> listOfTreasure : listOfTreasures) {
			String left = listOfTreasure.getLeft().toString();
			dictionary.put(left, listOfTreasure.getRight());
		}

		Integer lockPicking = dictionary.get("LockPicking");
		Integer strength = dictionary.get("Strength");
		Boolean isOpen = dictionary.get("LockIsOpen") == 1;

		boolean gold = dictionary.containsKey("Gold");
		String type;
		if (gold){
			type = "Gold";
		}else{
			type = "Diamond";
		}

		Integer capacity = dictionary.get(type);
		Tuple4<Integer, Integer, Integer, Boolean> treasureInfo = new Tuple4<>(capacity,lockPicking,strength,isOpen);
		allTreasures.addIfTreasure(nodeId,type,treasureInfo);

		if (gold){
			goldTreasures.addIfTreasure(nodeId,type,treasureInfo);
		} else{
			diamondTreasures.addIfTreasure(nodeId,type,treasureInfo);
		}
	}

	public synchronized ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getAllTreasures()
	{
		return allTreasures.getTreasures();
	}
	public synchronized ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getGoldTreasures()
	{
		return goldTreasures.getTreasures();
	}
	public synchronized ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getDiamondTreasures()
	{
		return diamondTreasures.getTreasures();
	}

	public synchronized ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getClosedTreasures()
	{
		return allTreasures.getClosedTreasures();
	}

	public synchronized void setIthGoldTreasure(int index, Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure)
	{
		this.goldTreasures.setIthTreasure(index, treasure);
	}
	public synchronized void setIthDiamondTreasure(int index, Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure)
	{
		this.diamondTreasures.setIthTreasure(index, treasure);
	}

	public synchronized AgentsInfo getAgentsinfo(){
		return agentsinfo;
	}
	public synchronized void addAgentFound(String agentName, HashMap<String, String> Info){
		agentsinfo.addAgent(agentName, Info);
	}

	public synchronized boolean alreadyInAgentsInfo(String nameAgent)
	{
		return agentsinfo.inAgentsInfo(nameAgent);
	}

	public synchronized HashMap<String, HashMap<String, String>> getAgentsFound()
	{
		return agentsinfo.getAgentsFound();
	}
	public synchronized String getCoordinatorPosition(){
		return agentsinfo.getAgentPosition("coordinator");
	}

	public void mergeTreasures(ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> trReceived) {
		ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> copyTreasures = new ArrayList<>(this.allTreasures.getTreasures());
		for (Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> newTreasure : trReceived) {
			String nodeIdNew = newTreasure.getFirst();
			Tuple4<Integer, Integer, Integer, Boolean> newTreasureInfo = newTreasure.getThird();

			boolean found = false;
			Tuple4<Integer, Integer, Integer, Boolean> oldTreasureInfo = null;
			for (Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> oldTreasure : copyTreasures) {
				String nodeIdOld = oldTreasure.getFirst();
				if (Objects.equals(nodeIdNew, nodeIdOld)) {
					found = true;
					oldTreasureInfo = oldTreasure.getThird();
					break;
				}
			}
			if (!found) {
				this.allTreasures.addTreasure(newTreasure.getFirst(), newTreasure.getSecond(), newTreasureInfo);
				if (Objects.equals(newTreasure.getSecond(), "Gold")) {
					goldTreasures.addTreasure(newTreasure.getFirst(), newTreasure.getSecond(), newTreasureInfo);
				} else { // Diamonds
					diamondTreasures.addTreasure(newTreasure.getFirst(), newTreasure.getSecond(), newTreasureInfo);
				}
			} else { // Update treasures
				String name = newTreasure.getSecond();
				int amount = Math.min(oldTreasureInfo.get_1(), newTreasureInfo.get_1());
				int lock = oldTreasureInfo.get_2();
				int strength = oldTreasureInfo.get_3();
				boolean isOpen = oldTreasureInfo.get_4() || newTreasureInfo.get_4();

				Tuple4<Integer, Integer, Integer, Boolean> newInfo = new Tuple4<>(amount,lock,strength,isOpen);
				Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> updatedTreasure =
						new Tuple3<>(nodeIdNew,name,newInfo);

				this.allTreasures.setNodeTreasure(nodeIdNew, updatedTreasure);
				if (Objects.equals(newTreasure.getSecond(), "Gold")) {
					goldTreasures.setNodeTreasure(nodeIdNew, updatedTreasure);
				} else { // Diamonds
					diamondTreasures.setNodeTreasure(nodeIdNew, updatedTreasure);
				}
			}
		}
	}

	public void mergeAgentsInfo(HashMap<String, HashMap<String, String>> AgReceived) {
		HashMap<String, HashMap<String, String>> copyAgents = new HashMap<>(this.agentsinfo.getAgentsFound());
		for (HashMap.Entry<String, HashMap<String, String>> newAgent : AgReceived.entrySet()) {
			String newAgentName = newAgent.getKey();
			boolean found = false;
			for(HashMap.Entry<String, HashMap<String, String>> oldAgent : copyAgents.entrySet()){
				String oldAgentName = oldAgent.getKey();
				if (Objects.equals(newAgentName, oldAgentName)) {
					found = true;
					break;
				}
			}
			if(!found){
				this.agentsinfo.addAgent(newAgentName, newAgent.getValue());
			}
		}
	}



	/** ALREADY IMPLEMENTED FUNCTIONS BY DEDALE */

	public void mergeMap(SerializableSimpleGraph<String, MapAttribute> sgreceived) {
		for (SerializableNode<String, MapAttribute> n: sgreceived.getAllNodes()){
			boolean alreadyIn =false;
			Node newnode=null;
			try {
				newnode=this.g.addNode(n.getNodeId());
			}	catch(IdAlreadyInUseException e) {
				alreadyIn=true;
			}
			if (!alreadyIn) {
				newnode.setAttribute("ui.class", n.getNodeContent().toString());
			}else{
				newnode=this.g.getNode(n.getNodeId());
				if (((String) newnode.getAttribute("ui.class"))==MapAttribute.closed.toString() || n.getNodeContent().toString()==MapAttribute.closed.toString()) {
					newnode.setAttribute("ui.class",MapAttribute.closed.toString());
				}
			}
		}

		for (SerializableNode<String, MapAttribute> n: sgreceived.getAllNodes()){
			for(String s:sgreceived.getEdges(n.getNodeId())){
				addEdge(n.getNodeId(),s);
			}
		}
	}

	public synchronized void addNode(String id, MapAttribute mapAttribute){
		Node n;
		if (this.g.getNode(id)==null){
			n=this.g.addNode(id);
		}else{
			n=this.g.getNode(id);
		}
		n.clearAttributes();
		n.setAttribute("ui.class", mapAttribute.toString());
	}

	public synchronized boolean addNewNode(String id) {
		if (this.g.getNode(id)==null){
			addNode(id,MapAttribute.open);
			return true;
		}
		return false;
	}

	public synchronized void addEdge(String idNode1,String idNode2){
		this.nbEdges++;
		try {
			this.g.addEdge(this.nbEdges.toString(), idNode1, idNode2);
		}catch (IdAlreadyInUseException e1) {
			System.err.println("ID existing");
			System.exit(1);
		}catch (EdgeRejectedException e2) {
			this.nbEdges--;
		} catch(ElementNotFoundException e3){

		}
	}

	public synchronized List<String> getShortestPath(String idFrom,String idTo){
		List<String> shortestPath=new ArrayList<String>();

		Dijkstra dijkstra = new Dijkstra();//number of edge
		dijkstra.init(g);
		dijkstra.setSource(g.getNode(idFrom));
		dijkstra.compute();//compute the distance to all nodes from idFrom
		List<Node> path=dijkstra.getPath(g.getNode(idTo)).getNodePath(); //the shortest path from idFrom to idTo
		for (Node edges : path) {
			shortestPath.add(edges.getId());
		}
		dijkstra.clear();
		if (shortestPath.isEmpty()) {//The openNode is not currently reachable
			return null;
		}else {
			shortestPath.remove(0);//remove the current position
		}
		return shortestPath;
	}

	public synchronized List<String> getShortestPathWithoutNode(String idFrom,String idTo,String myId){
		List<String> shortestPath=new ArrayList<String>();

		if (g.getNode(myId) != null) {
			Stream<Edge> edges = g.getNode(myId).edges();
			edges.forEach(x -> g.removeEdge(x));
			g.removeNode(myId);
		}

		Dijkstra dijkstra = new Dijkstra();//number of edge
		dijkstra.init(g);
		dijkstra.setSource(g.getNode(idFrom));
		dijkstra.compute();//compute the distance to all nodes from idFrom
		List<Node> path=dijkstra.getPath(g.getNode(idTo)).getNodePath(); //the shortest path from idFrom to idTo
		for (Node edges : path) {
			shortestPath.add(edges.getId());
		}
		dijkstra.clear();
		if (shortestPath.isEmpty()) {//The openNode is not currently reachable
			return null;
		}else {
			shortestPath.remove(0);//remove the current position
		}
		return shortestPath;
	}

	public List<String> getShortestPathToClosestOpenNode(String myPosition) {
		List<String> opennodes=getOpenNodes();

		List<Couple<String,Integer>> lc=
				opennodes.stream()
						.map(on -> (getShortestPath(myPosition,on)!=null)? new Couple<String, Integer>(on,getShortestPath(myPosition,on).size()): new Couple<String, Integer>(on,Integer.MAX_VALUE))//some nodes my be unreachable if the agents do not share at least one common node.
						.collect(Collectors.toList());

		Optional<Couple<String,Integer>> closest=lc.stream().min(Comparator.comparing(Couple::getRight));

		return getShortestPath(myPosition,closest.get().getLeft());
	}

	public List<String> getOpenNodes(){
		return this.g.nodes()
				.filter(x ->x .getAttribute("ui.class")==MapAttribute.open.toString())
				.map(Node::getId)
				.collect(Collectors.toList());
	}

	public void prepareMigration(){
		serializeGraphTopology();
		closeGui();
		this.g=null;
	}

	private void serializeGraphTopology() {
		this.sg= new SerializableSimpleGraph<String,MapAttribute>();
		Iterator<Node> iter=this.g.iterator();
		while(iter.hasNext()){
			Node n=iter.next();
			sg.addNode(n.getId(),MapAttribute.valueOf((String)n.getAttribute("ui.class")));
		}
		Iterator<Edge> iterE=this.g.edges().iterator();
		while (iterE.hasNext()){
			Edge e=iterE.next();
			Node sn=e.getSourceNode();
			Node tn=e.getTargetNode();
			sg.addEdge(e.getId(), sn.getId(), tn.getId());
		}
	}

	public synchronized SerializableSimpleGraph<String,MapAttribute> getSerializableGraph(){
		serializeGraphTopology();
		return this.sg;
	}

	private synchronized void closeGui() {
		if (this.viewer!=null){
			try{
				this.viewer.close();
			}catch(NullPointerException e){
				System.err.println("Bug graphstream viewer.close() work-around - https://github.com/graphstream/gs-core/issues/150");
			}
			this.viewer=null;
		}
	}

	private synchronized void openGui() {
		this.viewer =new FxViewer(this.g, FxViewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);//GRAPH_IN_GUI_THREAD)
		viewer.enableAutoLayout();
		viewer.setCloseFramePolicy(FxViewer.CloseFramePolicy.CLOSE_VIEWER);
		viewer.addDefaultView(true);
		g.display();
	}

	public boolean hasOpenNode() {
		return (this.g.nodes()
				.filter(n -> n.getAttribute("ui.class")==MapAttribute.open.toString())
				.findAny()).isPresent();
	}
}