package eu.su.mas.dedaleEtu.mas.agents;

import java.util.*;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;

import eu.su.mas.dedaleEtu.mas.behaviours.*;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;

import dataStructures.tuple.Couple;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

public class ExplorerAgent extends AbstractDedaleAgent {

    private static final long serialVersionUID = -7969469610241668140L;
    private MapRepresentation myMap;
    private String startPosition;
    private ArrayList<String> myRoute;
    private String myAction;
    private ArrayList<String> inverseRoute;
    private ArrayList<Couple<String, String>> assignedAgents; // Name and node

    private Couple<String, String> facilitatorToBeInformed = null;

    private String treasureToOpenPosition = null;

    private final ArrayList<String> receivers = new ArrayList<>();
    private int stuck = 0;
    private boolean wait = false;

    protected void setup()
    {
        super.setup();
        this.myRoute = new ArrayList<>();
        this.myAction = null;
        this.inverseRoute = new ArrayList<>();

        //get the parameters added to the agent at creation (if any)
        final Object[] args = getArguments();

        if(args.length==0){
            System.err.println("Error while creating the agent, names of agent to contact expected");
            System.exit(-1);
        }else{
            int i=2;// WARNING YOU SHOULD ALWAYS START AT 2. This will be corrected in the next release.
            while (i<args.length) {
                this.receivers.add((String)args[i]);
                i++;
            }
        }

        List<Behaviour> lb = new ArrayList<>();
        FSMBehaviour fsm = new FSMBehaviour();

        fsm.registerFirstState(new ExplorationBehaviour(this), "exploration");
        fsm.registerState(new ShareInfoBehaviour(fsm, this, "Explorer"), "shareInfo");
        fsm.registerState(new PassThroughSendBehaviour(fsm,this, "Explorer"), "passSend");
        fsm.registerState(new PassThroughReceiveBehaviour(fsm, this, "Explorer"), "passReceive");

        fsm.registerState(new ShareCoordBehaviour(this), "shareCoord");
        fsm.registerState(new MoveToKnownPositionBehaviour(this), "moveToKnown");
        fsm.registerState(new WaitForActionBehaviour(this,"Explorer"), "wait");
        fsm.registerState(new MoveToNodeBehaviour( this, "Explorer"), "moveToNode");
        fsm.registerState(new DoActionBehaviour(this,"Explorer"), "doAction");

        // Exploration
        fsm.registerDefaultTransition("exploration", "shareInfo");
        fsm.registerTransition("exploration", "passSend", 1);
        fsm.registerTransition("exploration", "shareCoord", 3);

        // Share info
        fsm.registerDefaultTransition("shareInfo", "exploration");
        fsm.registerTransition("shareInfo", "shareCoord", 3);
        fsm.registerTransition("shareInfo","passSend", 4);

        // Share info with coords
        fsm.registerDefaultTransition("shareCoord", "moveToNode");
        fsm.registerTransition("shareCoord", "moveToKnown", 1);

        // Pass through send
        fsm.registerDefaultTransition("passSend", "passReceive");

        // Pass through receive
        fsm.registerDefaultTransition("passReceive", "exploration");
//        fsm.registerTransition("passReceive", "wait", 3);
        fsm.registerTransition("passReceive", "moveToNode", 4);
        fsm.registerTransition("passReceive", "moveToNode", 3);

        // Move to known
        fsm.registerDefaultTransition("moveToKnown", "moveToNode");

        // Move to node
        fsm.registerDefaultTransition("moveToNode", "moveToNode");
        fsm.registerTransition("moveToNode", "doAction", 2);
        fsm.registerTransition("moveToNode", "shareInfo", 3);
        fsm.registerTransition("moveToNode", "passSend", 4);
        fsm.registerTransition("moveToNode", "wait", 5);

        // Do action
        fsm.registerDefaultTransition("doAction", "moveToNode");
        fsm.registerTransition("doAction", "passReceive",2);

        // Wait
        fsm.registerDefaultTransition("wait", "passReceive");
//        fsm.registerTransition("wait", "moveToNode", 2);

        // Use this behaviour
        lb.add(fsm);
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("The agent "+this.getLocalName()+ " is started");
    }

    public MapRepresentation getMyMap()
    {
        return myMap;
    }

    public void setMyMap(MapRepresentation myMap) {
        this.myMap = myMap;
    }

    public String getStartPosition() {
        return startPosition;
    }

    public ArrayList<String> getMyRoute() {
        return myRoute;
    }

    public String getMyAction()
    {
        return this.myAction;
    }

    public void setMyAction(String myAction)
    {
        this.myAction = myAction;
    }

    public void setMyRoute(ArrayList<String> myRoute) {
        this.myRoute = new ArrayList<>(myRoute);
    }

    public void addToMyRoute(String node) {
        this.myRoute.add(0,node);
    }

    public void addToInverseRoute(String node) {
        this.inverseRoute.add(0, node);
    }

    public ArrayList<String> getInverseRoute() {
        return inverseRoute;
    }

    public ArrayList<String> getReceivers() {
        return receivers;
    }

    public ArrayList<Couple<String, String>> getAssignedAgents()
    {
        return assignedAgents;
    }


    public void addAssignedAgents(String name, String new_pos){
        Couple<String, String> name_pos = new Couple<>(name, new_pos);
        if (assignedAgents != null) {
            if (!assignedAgents.contains(name_pos)) {
                System.out.println(this.getLocalName()+": Adding to assigned agents \""+name+ "\" on "+new_pos);
                assignedAgents.add(name_pos);
            }
            return;
        }
        assignedAgents = new ArrayList<>(Collections.singletonList(name_pos));
        System.out.println(this.getLocalName()+": Adding to assigned agents "+name+ " on "+new_pos);
    }

    public void addAssignedAgentsFromHashMap(HashMap<String, HashMap<String, String>> AgReceived){
        HashMap<String, HashMap<String, String>> copyAgents = new HashMap<>(this.myMap.getAgentsFound());
        for (HashMap.Entry<String, HashMap<String, String>> newAgent : AgReceived.entrySet()) {
            String newAgentName = newAgent.getKey();
            if(!copyAgents.containsKey(newAgentName)){
                // Only add the coordinators
                if (newAgentName.contains("coord")) {
//                    System.out.println("Added " + newAgentName + " at " + newAgent.getValue().get("Position"));
                    this.addAssignedAgents(newAgentName, newAgent.getValue().get("Position"));
                }
            }
        }

    }

    public void eraseFirstPendingInformationPos(){
        assignedAgents.remove(0);
    }
    public void setStartPosition(String startPosition)
    {
        this.startPosition = startPosition;
    }

    public void setFacilitatorToBeInformed(Couple <String, String> name_pos) {
        facilitatorToBeInformed = name_pos;
    }
    public Couple<String,String> getFacilitatorToBeInformed() {
        return facilitatorToBeInformed;
    }

    public void setTreasureToOpenPosition(String pos) {
        treasureToOpenPosition = pos;
    }
    public String getTreasureToOpenPosition() {
        return treasureToOpenPosition;
    }

    public boolean getSingingStatus() {
        return false;
    }

    public int getStuck() {
        return stuck;
    }

    public void setStuck(int stuck) {
        this.stuck = stuck;
    }

    public boolean getWait() {
        return wait;
    }

    public void setWait(boolean wait) {
        this.wait = wait;
    }
}
