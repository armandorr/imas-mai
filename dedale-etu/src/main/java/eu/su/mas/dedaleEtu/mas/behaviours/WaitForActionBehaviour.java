package eu.su.mas.dedaleEtu.mas.behaviours;

import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.*;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.IOException;
import java.util.*;

import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class WaitForActionBehaviour extends OneShotBehaviour {
    /**
     *
     */

    private final String[] receivers = {"explorer1","explorer2","explorer3","collectorGold1","collectorGold2",
            "collectorDiamond1","collectorDiamond2","tanker1","tanker2","coordinator","facilitatorDiamond","facilitatorGold"};

//    private final boolean sing;
    private MapRepresentation myMap;
    private int exitValue = 1;
    private String typeOfAgent = "";

    /**
     * @param a the agent
     */
    public WaitForActionBehaviour(Agent a, String typeOfAgent)
    {
        super(a);
        this.typeOfAgent = typeOfAgent;

        if (Objects.equals(this.typeOfAgent, "Coord")){
            this.myMap = ((CoordinatorAgent) this.myAgent).getMyMap();
        }
    }

    @Override
    public void action() {

        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean singingAllowed = true;
        this.exitValue = 3; // To receivePassThrough


        if (Objects.equals(this.typeOfAgent, "Collector")){
            String startPosition = ((CollectorAgent) this.myAgent).getStartPosition();
            if (startPosition == null) {
                startPosition = ((AbstractDedaleAgent) (this.myAgent)).getCurrentPosition();
                ((CollectorAgent) this.myAgent).setStartPosition(startPosition);
                ((CollectorAgent) this.myAgent).setMyRoute(new ArrayList<>()); // Empty list
            }
            singingAllowed = ((CollectorAgent) this.myAgent).getSingingStatus();
        }
        if (Objects.equals(this.typeOfAgent, "Coord")){
            if(this.myMap == null) {
                this.myMap = new MapRepresentation();
                ((CoordinatorAgent)this.myAgent).setMyMap(this.myMap);
            }
            String startPosition = ((CoordinatorAgent) this.myAgent).getStartPosition();
            if (startPosition == null) {
                startPosition = ((AbstractDedaleAgent) (this.myAgent)).getCurrentPosition();
                ((CoordinatorAgent) this.myAgent).setStartPosition(startPosition);
                ((CoordinatorAgent) this.myAgent).setMyRoute(new ArrayList<>()); // Empty list
            }
            singingAllowed = ((CoordinatorAgent) this.myAgent).getSingingStatus();
        }
        if (Objects.equals(this.typeOfAgent, "Fac")){
            String startPosition = ((FacilitatorAgent) this.myAgent).getStartPosition();
            if (startPosition == null) {
                startPosition = ((AbstractDedaleAgent) (this.myAgent)).getCurrentPosition();
                ((FacilitatorAgent) this.myAgent).setStartPosition(startPosition);
                ((FacilitatorAgent) this.myAgent).setMyRoute(new ArrayList<>()); // Empty list
            }
            singingAllowed = ((FacilitatorAgent) this.myAgent).getSingingStatus();
        }
        if (Objects.equals(this.typeOfAgent, "Tanker")){
            String startPosition = ((TankerAgent) this.myAgent).getStartPosition();
            if (startPosition == null) {
                startPosition = ((AbstractDedaleAgent) (this.myAgent)).getCurrentPosition();
                ((TankerAgent) this.myAgent).setStartPosition(startPosition);
                ((TankerAgent) this.myAgent).setMyRoute(new ArrayList<>()); // Empty list
            }
            singingAllowed = ((TankerAgent) this.myAgent).getSingingStatus();
        }
        if (Objects.equals(this.typeOfAgent, "Explorer"))
            singingAllowed = ((ExplorerAgent) this.myAgent).getSingingStatus();

        //before doing anything if singing is allowed
        if (singingAllowed) {
            MessageTemplate msgTemplateAction = MessageTemplate.and(
                    MessageTemplate.MatchProtocol("STOP-SINGING"),
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM));

            List<ACLMessage> msgSingList = this.myAgent.receive(msgTemplateAction, 1000);

            if (msgSingList != null) {
                ACLMessage msgSingStatus = msgSingList.get(0);
                try {
                    singingAllowed = (boolean) msgSingStatus.getContentObject();
                    System.out.println("I am "+this.myAgent.getLocalName()+" and I have received a message telling me to sing = "+singingAllowed);
                } catch (UnreadableException e) {
                    e.printStackTrace();
                }
                if (Objects.equals(this.typeOfAgent, "Collector"))
                    ((CollectorAgent) this.myAgent).setSingingStatus(singingAllowed);
                if (Objects.equals(this.typeOfAgent, "Coord"))
                    ((CoordinatorAgent) this.myAgent).setSingingStatus(singingAllowed);
                if (Objects.equals(this.typeOfAgent, "Fac"))
                    ((FacilitatorAgent) this.myAgent).setSingingStatus(singingAllowed);
                if (Objects.equals(this.typeOfAgent, "Tanker"))
                    ((TankerAgent) this.myAgent).setSingingStatus(singingAllowed);
            }
        }


        //Sing position
        if (singingAllowed){
            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.setSender(this.myAgent.getAID());
            msg.setProtocol("SING");
            for (String agentName : this.receivers) {
                if (!Objects.equals(agentName, this.myAgent.getLocalName()))
                    msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
            }

            AbstractDedaleAgent agent = ((AbstractDedaleAgent)this.myAgent);
            String myPosition = "";
            if (Objects.equals(typeOfAgent, "Collector")){ // Collector
                myPosition = ((CollectorAgent)this.myAgent).getStartPosition();
            } else if (Objects.equals(typeOfAgent, "Coord")){ // Coordinator
                myPosition = ((CoordinatorAgent)this.myAgent).getStartPosition();
            }else if (Objects.equals(typeOfAgent, "Fac")){ // Coordinator
                myPosition = ((FacilitatorAgent)this.myAgent).getStartPosition();
            }else if (Objects.equals(typeOfAgent, "Tanker")){ // Coordinator
                myPosition = ((TankerAgent)this.myAgent).getStartPosition();
            }
            String treasureType = agent.getMyTreasureType().getName();
            String nameAgent = this.myAgent.getLocalName();

            HashMap<String, String> listOfProperties = new HashMap<String, String>();
            listOfProperties.put("Name", this.myAgent.getLocalName());
            listOfProperties.put("Position", myPosition);
            listOfProperties.put("TreasureType", treasureType);

            Set<Couple<Observation,Integer>> expertises = agent.getMyExpertise();
            for (Couple<Observation,Integer> expertise: expertises) {
                String property = expertise.getLeft().getName();
                Integer value = expertise.getRight();
                listOfProperties.put(property, ""+value);
            }
//
            List<Couple<Observation,Integer>> capacities = agent.getBackPackFreeSpace();
            for (Couple<Observation,Integer> capacity: capacities) {
                String bagType = capacity.getLeft().getName();
                Integer remainingCapacity = capacity.getRight();
                listOfProperties.put(bagType+"Bag", ""+remainingCapacity);
            }

            Couple<String, HashMap<String, String>> InfoOfOneAgent = new Couple<String, HashMap<String, String>>(nameAgent, listOfProperties);

            try {
                msg.setContentObject(InfoOfOneAgent);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
        }

        if (Objects.equals(typeOfAgent, "Fac")) {
            MessageTemplate msgTemplate = MessageTemplate.and(
            MessageTemplate.MatchProtocol("SENDING-ROUTE"),
            MessageTemplate.MatchPerformative(ACLMessage.INFORM));
            List<ACLMessage> msgRouteList = this.myAgent.receive(msgTemplate,1000);
            if (msgRouteList != null) {
                ACLMessage messageInfo = msgRouteList.get(0);

                ACLMessage confirmationMsg = new ACLMessage(ACLMessage.INFORM);
                confirmationMsg.setProtocol("SENDING-ROUTE");

                confirmationMsg.setSender(this.myAgent.getAID());
                confirmationMsg.addReceiver(new AID(messageInfo.getSender().getLocalName(), AID.ISLOCALNAME));

                try {
                    confirmationMsg.setContentObject("Recieved");
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                ((AbstractDedaleAgent) this.myAgent).sendMessage(confirmationMsg);


                ArrayList<String> infoReceived = null;
                try {
                    infoReceived = (ArrayList<String>) messageInfo.getContentObject();
                } catch (UnreadableException e) {
                    e.printStackTrace();
                }
                ((FacilitatorAgent) this.myAgent).setMyRoute(infoReceived);
                ((FacilitatorAgent) this.myAgent).setExplorationPhase(false);
                this.exitValue = 2;
                try {
                    System.out.println(this.myAgent.getLocalName()+": Waiting 25 seconds");
                    this.myAgent.doWait(25000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else {
            //Receive action
            MessageTemplate msgTemplateAction = MessageTemplate.and(
                    MessageTemplate.MatchProtocol("DO-ACTION"),
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM));

            List<ACLMessage> msgActionList = this.myAgent.receive(msgTemplateAction, 1000);

            if (msgActionList != null) {
                ACLMessage msgAction = msgActionList.get(0);
                Tuple4<ArrayList<String>, Couple<String,Integer>, ArrayList<String>, ArrayList<String>> action = null;
                try {
                    action = (Tuple4<ArrayList<String>, Couple<String,Integer>, ArrayList<String>, ArrayList<String>>) msgAction.getContentObject();
                } catch (UnreadableException e) {
                    e.printStackTrace();
                }
                ArrayList<String> myRoute = action.get_1();
                String myAction = action.get_2().getLeft();
                ArrayList<String> myRouteToTanker = action.get_3();
                ArrayList<String> inverseRoute = action.get_4();

                if (Objects.equals(this.typeOfAgent, "Explorer")) {
                    ((ExplorerAgent) this.myAgent).setMyRoute(myRoute);
                    ((ExplorerAgent) this.myAgent).setMyAction(myAction);
                    this.exitValue = 3; // To moveToNode
                } else { // Collector
                    Couple<String, Integer> prevTr = ((CollectorAgent) this.myAgent).getPrevTr();
                    this.exitValue = 2; // To moveToNode
                    ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
                    msg_route.setProtocol("COLLECTOR-OK");
                    msg_route.setSender(this.myAgent.getAID());
                    for (String agentName : this.receivers) {
                        msg_route.addReceiver(new AID(agentName, AID.ISLOCALNAME));
                    }
                    ((AbstractDedaleAgent) this.myAgent).sendMessage(msg_route);
                    try {
                        this.myAgent.doWait(5000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (Objects.equals(prevTr.getRight(), action.get_2().getRight()) && Objects.equals(myRoute.get(myRoute.size() - 1), prevTr.getLeft())) return;
                    Couple<String,Integer> nextTr = new Couple<>(myRoute.get(myRoute.size() - 1),action.get_2().getRight());
                    ((CollectorAgent) this.myAgent).setPrevTr(nextTr);
                    ((CollectorAgent) this.myAgent).setMyRoute(myRoute);
                    ((CollectorAgent) this.myAgent).setMyAction(myAction);
                    ((CollectorAgent) this.myAgent).setMyRouteToTanker(myRouteToTanker);
                    ((CollectorAgent) this.myAgent).setInverseRoute(inverseRoute);
                    this.myAgent.receive(msgTemplateAction, 1000);
                }

            }
        }
        if (Objects.equals(this.typeOfAgent, "Explorer")) {
            ((ExplorerAgent) this.myAgent).setWait(true);
        }
    }

    @Override
    public int onEnd()
    {
        return this.exitValue;
    }

}
