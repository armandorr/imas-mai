package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;

import eu.su.mas.dedaleEtu.mas.behaviours.MoveToNodeBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.PassThroughReceiveBehaviour;
import eu.su.mas.dedaleEtu.mas.behaviours.WaitForActionBehaviour;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

public class TankerAgent extends AbstractDedaleAgent {
    private String startPosition = null;
    private ArrayList<String> myRoute;
    private ArrayList<String> inverseRoute;
    private boolean singingStatus = true;
    protected void setup(){

        super.setup();
        this.myRoute = new ArrayList<>();
        this.inverseRoute = new ArrayList<>();

        List<Behaviour> lb=new ArrayList<Behaviour>();
        FSMBehaviour fsm = new FSMBehaviour();

        fsm.registerFirstState(new WaitForActionBehaviour(this,"Tanker"), "singAndWait");
        fsm.registerState(new PassThroughReceiveBehaviour(fsm, this, "Tanker"), "passReceive");
        fsm.registerState(new MoveToNodeBehaviour(this, "Tanker"), "moveToNode");

        fsm.registerDefaultTransition("singAndWait", "passReceive");
        fsm.registerDefaultTransition("passReceive", "moveToNode");
        fsm.registerDefaultTransition("moveToNode", "moveToNode");
        fsm.registerTransition("moveToNode", "passReceive", 4);
        fsm.registerTransition("moveToNode", "singAndWait", 2);

        lb.add(fsm);
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("The  agent "+this.getLocalName()+ " is started");
    }

    public boolean getSingingStatus() {
        return this.singingStatus;
    }

    public void setSingingStatus(boolean status) {
        this.singingStatus = status;
    }

    public ArrayList<String> getInverseRoute() {
        return inverseRoute;
    }

    public void setInverseRoute(ArrayList<String> inverseRoute) {
        this.inverseRoute = inverseRoute;
    }

    public ArrayList<String> getMyRoute() {
        return myRoute;
    }

    public void setMyRoute(ArrayList<String> myRoute) {
        this.myRoute = myRoute;
    }

    public String getStartPosition() {
        return startPosition;
    }
    public void addToMyRoute(String node) {
        this.myRoute.add(0,node);
    }

    public void setStartPosition(String startPosition) {
        this.startPosition = startPosition;
    }
    public void addToInverseRoute(String node) {
        this.inverseRoute.add(0, node);
    }
}
