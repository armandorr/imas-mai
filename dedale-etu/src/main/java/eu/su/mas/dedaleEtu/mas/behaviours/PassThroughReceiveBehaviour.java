package eu.su.mas.dedaleEtu.mas.behaviours;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.*;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static eu.su.mas.dedaleEtu.princ.Principal.STEPS;
import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class PassThroughReceiveBehaviour extends SimpleBehaviour {
    /**
     *
     */
    private boolean finished = false;
    private final FSMBehaviour fsm;
    private String typeOfAgent;

    /**
     * @param a the agent
     */
    public PassThroughReceiveBehaviour(FSMBehaviour fsm, Agent a, String typeOfAgent)
    {
        super(a);
        this.fsm = fsm;
        this.typeOfAgent = typeOfAgent;
    }
    private void removeRedundantMovements() {
        ArrayList<String> path = null;
        if (Objects.equals(this.typeOfAgent, "Explorer")) {
            path = ((ExplorerAgent) this.myAgent).getMyRoute();
        } else if (Objects.equals(this.typeOfAgent, "Collector")) {
            path = ((CollectorAgent) this.myAgent).getMyRoute();
        } else if (Objects.equals(this.typeOfAgent, "Coord")) {
            path = ((CoordinatorAgent) this.myAgent).getMyRoute();
        } else if (Objects.equals(this.typeOfAgent, "Fac")) {
            path = ((FacilitatorAgent) this.myAgent).getMyRoute();
        } else if (Objects.equals(this.typeOfAgent, "Tanker")) {
            path = ((TankerAgent) this.myAgent).getMyRoute();
        }
        int size = path.size();
        for (int i = 0; i < size; i++) {
            for (int j = size-1; j >= 0; --j) {
                if (Objects.equals(path.get(i), path.get(j))) {
                    path.subList(i+1, j+1).clear();
                    size = path.size();
                    if (j >= size) j = size-1;
                }
            }
        }
        if (Objects.equals(this.typeOfAgent, "Explorer")) {
            ((ExplorerAgent) this.myAgent).setMyRoute(path);
        } else if (Objects.equals(this.typeOfAgent, "Collector")) {
            ((CollectorAgent) this.myAgent).setMyRoute(path);
        } else if (Objects.equals(this.typeOfAgent, "Coord")) {
            ((CoordinatorAgent) this.myAgent).setMyRoute(path);
        } else if (Objects.equals(this.typeOfAgent, "Fac")) {
            ((FacilitatorAgent) this.myAgent).setMyRoute(path);
        } else if (Objects.equals(this.typeOfAgent, "Tanker")) {
            ((TankerAgent) this.myAgent).setMyRoute(path);
        }
    }

    private void moveRandom() {
        Random rand = new Random();
        int randomNum = 5 + rand.nextInt(25-5+1);
        ArrayList<String> tmp = new ArrayList<>();
        for (int i = 0; i < randomNum; ++i) {
            List<Couple<String,List<Couple<Observation,Integer>>>> lobs = ((AbstractDedaleAgent)this.myAgent).observe();
            String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

            boolean posChosen = false;
            Couple<String, List<Couple<Observation, Integer>>> randomElement = null;
            int j = 0;
            while (!posChosen && j < 10) {
                randomElement = lobs.get(rand.nextInt((lobs.size())));
                String nodeId = randomElement.getLeft();
                if (!Objects.equals(myPosition, nodeId)) posChosen = true;
                j+=1;
            }

            String nextMovement = randomElement.getLeft();

            try {
                this.myAgent.doWait(500);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!tmp.contains(nextMovement) && ((AbstractDedaleAgent)this.myAgent).moveTo(nextMovement)) {
                ++STEPS;
                tmp.add(nextMovement);
                if (Objects.equals(this.typeOfAgent, "Explorer")){
                    ((ExplorerAgent)this.myAgent).addToMyRoute(myPosition);
                }
                else if (Objects.equals(this.typeOfAgent, "Collector")){
                    ((CollectorAgent)this.myAgent).addToMyRoute(myPosition);
                }
                else if (Objects.equals(this.typeOfAgent, "Coord")){
                    ((CoordinatorAgent)this.myAgent).addToMyRoute(myPosition);
                }
                else if (Objects.equals(this.typeOfAgent, "Fac")){
                    ((FacilitatorAgent)this.myAgent).addToMyRoute(myPosition);
                }
                else if (Objects.equals(this.typeOfAgent, "Tanker")){
                    ((TankerAgent)this.myAgent).addToMyRoute(myPosition);
                }
            }
        }
        try {
            this.myAgent.doWait(rand.nextInt((5000 - 2000) + 1000) + 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean moveApart(Couple<ArrayList<String>,String> routePosition){
        ArrayList<String> route = routePosition.getLeft();
        String position = routePosition.getRight();
        ArrayList<String> visitedNodes = new ArrayList<>();
        visitedNodes.add(position);


        List<Couple<String,List<Couple<Observation,Integer>>>> lobs = ((AbstractDedaleAgent)this.myAgent).observe();
        String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

        String nextMovement = "";

        for (Couple<String, List<Couple<Observation, Integer>>> value : lobs) {
            String nodeId = value.getLeft();
            if (!Objects.equals(myPosition, nodeId) && !visitedNodes.contains(nodeId)) {
                if (Objects.equals(nextMovement, "")) {
                    nextMovement = nodeId;
                }
                if (!route.contains(nodeId)){
                    nextMovement = nodeId;
                    break;
                }
            }
        }
        if (Objects.equals(nextMovement, "")){
            return false;
        }

        try {
            this.myAgent.doWait(500);
        } catch (Exception e) {
            e.printStackTrace();
        }

        visitedNodes.add(myPosition);
        if (((AbstractDedaleAgent)this.myAgent).moveTo(nextMovement)) {
            ++STEPS;
            if (Objects.equals(this.typeOfAgent, "Explorer")) {
                ((ExplorerAgent) this.myAgent).addToMyRoute(myPosition);
            } else if (Objects.equals(this.typeOfAgent, "Collector")) {
                ((CollectorAgent) this.myAgent).addToMyRoute(myPosition);
            } else if (Objects.equals(this.typeOfAgent, "Coord")) {
                ((CoordinatorAgent) this.myAgent).addToMyRoute(myPosition);
            } else if (Objects.equals(this.typeOfAgent, "Fac")) {
                ((FacilitatorAgent) this.myAgent).addToMyRoute(myPosition);
            } else if (Objects.equals(this.typeOfAgent, "Tanker")) {
                ((TankerAgent) this.myAgent).addToMyRoute(myPosition);
            }
        }

        Random rand = new Random();

        try {
            this.myAgent.doWait(rand.nextInt((5000 - 2000) + 1000) + 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void action() {
        //Receive route
        MessageTemplate msgTemplateRandom = MessageTemplate.and(
                MessageTemplate.MatchProtocol("PASS-THROUGH-RANDOM"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM));

        List<ACLMessage> msgRouteListRandom = this.myAgent.receive(msgTemplateRandom,1000);

        if (msgRouteListRandom != null) {
            System.out.println("I have received to move random. I'll do it.");
            moveRandom();
            finished = true;
            removeRedundantMovements();
        }

        else {

            MessageTemplate msgTemplate = MessageTemplate.and(
                    MessageTemplate.MatchProtocol("PASS-THROUGH"),
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM));

            List<ACLMessage> msgRouteList = this.myAgent.receive(msgTemplate, 1000);


            if (msgRouteList != null) {
                ACLMessage msgRoute = msgRouteList.get(0);
                Couple<ArrayList<String>, String> routeReceived = null;
                try {
                    routeReceived = (Couple<ArrayList<String>, String>) msgRoute.getContentObject();
                } catch (UnreadableException e) {
                    e.printStackTrace();
                }
                // If there is a message, finish only if achieved moving apart
                finished = moveApart(routeReceived);
                removeRedundantMovements();
                // If not finish do n random move
            } else {
                // If no message, finish behaviour
                finished = true;
            }
        }
        this.myAgent.receive(msgTemplateRandom,1000);
        finished = true;
    }

    @Override
    public int onEnd()
    {
        // Finish with the same exit value as entered to know where to return in the next state
        return this.fsm.getLastExitValue();
    }

    @Override
    public boolean done()
    {
        return finished;
    }

}
