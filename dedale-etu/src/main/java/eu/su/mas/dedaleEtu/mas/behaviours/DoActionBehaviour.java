package eu.su.mas.dedaleEtu.mas.behaviours;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static eu.su.mas.dedale.env.Observation.ANY_TREASURE;
import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class DoActionBehaviour extends OneShotBehaviour {
    private String typeOfAgent = "";

    /** Possible actions:
     *      Pick
     *      Lock-Gold
     *      Lock-Diamond
     *      Give-{"tankerName"}
     **/

    /**
     * @param a the agent
     */
    public DoActionBehaviour(Agent a, String typeOfAgent)
    {
        super(a);
        this.typeOfAgent = typeOfAgent;
    }

    @Override
    public void action() {

        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String [] pickActionsS = {"gold", "diamond"};
        List<String> pickActions = Arrays.asList(pickActionsS);
        String action;

        // We should only enter here if we have an action

        if (Objects.equals(this.typeOfAgent, "Collector")){ // The only agent that should enter here

            CollectorAgent agent = ((CollectorAgent)this.myAgent);
            action = agent.getMyAction();

            if (pickActions.contains(action)){ // We have to do lock and then pick
                boolean openLock = false;
                while (!openLock) {
                    Observation type = (Objects.equals(action, "gold")) ? Observation.GOLD: Observation.DIAMOND;
                    openLock = agent.openLock(type);
                    System.out.println(this.myAgent.getLocalName() + ": I'm unlocking the treasure at "+((CollectorAgent) this.myAgent).getCurrentPosition());
                    try {
                        this.myAgent.doWait(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                int pickedQuantity = 0;
                while (pickedQuantity == 0) {
                    System.out.println(this.myAgent.getLocalName() + ": I'm picking the treasure at "+((CollectorAgent) this.myAgent).getCurrentPosition());
                    try {
                        this.myAgent.doWait(2000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pickedQuantity = agent.pick();
                }

                agent.setMyAction("give");
                agent.setMyRoute(agent.getMyRouteToTanker());

                System.out.println(this.myAgent.getLocalName() + ": Treasure is picked up!");

            } else { // Giving the treasure to the tanker
                boolean treasureGived = false;
                while (!treasureGived){
                    treasureGived = agent.emptyMyBackPack("tanker1");
                    if (!treasureGived)
                        treasureGived = agent.emptyMyBackPack("tanker2");
                }

                agent.setMyAction(null);
                ArrayList<String> inverseRoute = agent.getInverseRoute();
                agent.setMyRoute(inverseRoute);
                System.out.println(this.myAgent.getLocalName() + ": Treasure given to the tanker, going back home");
            }
        }
    }
}
