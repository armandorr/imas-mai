package eu.su.mas.dedaleEtu.mas.agents;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.core.AID;


/**
 * Dummy Tanker agent. It does nothing more than printing what it observes every 10s and receiving the treasures from other agents. 
 * <br/>
 * Note that this last behaviour is hidden, every tanker agent automatically possess it.
 * 
 * @author hc
 *
 */
public class DummySingerAgent extends AbstractDedaleAgent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1784844593772918359L;



	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){

		super.setup();

		super.setup();

		//get the parameters added to the agent at creation (if any)
		final Object[] args = getArguments();

		List<String> list_recievers=new ArrayList<String>();

		if(args.length==0){
			System.err.println("Error while creating the agent, names of agent to contact expected");
			System.exit(-1);
		}else{
			int i=2;// WARNING YOU SHOULD ALWAYS START AT 2. This will be corrected in the next release.
			while (i<args.length) {
				list_recievers.add((String)args[i]);
				i++;
			}
		}

		List<Behaviour> lb=new ArrayList<Behaviour>();
//		lb.add(new RandomTankerBehaviour(this));
		lb.add(new SingerBehaviour(this, list_recievers));

		addBehaviour(new startMyBehaviours(this,lb));
		
		System.out.println("The  agent "+this.getLocalName()+ " is singing his name and position");

	}

	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){

	}
}


/**************************************
 * 
 * 
 * 				BEHAVIOUR
 * 
 * 
 **************************************/

class SingerBehaviour extends TickerBehaviour{

	/**
	 *
	 */
	private static final long serialVersionUID = -2058134622078521998L;

	private List<String> receivers;

	/**
	 * An agent tries to contact its friend and to give him its current position
	 * @param myagent the agent who posses the behaviour
	 *
	 */
	public SingerBehaviour (final AbstractDedaleAgent myagent, List<String> receivers) {
		super(myagent, 3000);
		this.receivers=receivers;
		//super(myagent);
	}

	@Override
	public void onTick() {
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		//A message is defined by : a performative, a sender, a set of receivers, (a protocol),(a content (and/or contentOBject))
		ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("AGENTSINFO_protocol");
		AbstractDedaleAgent agente = ((AbstractDedaleAgent)this.myAgent);
		if (myPosition!=""){
			//System.out.println("Agent "+this.myAgent.getLocalName()+ " is trying to reach its friends");

			String treasureType = agente.getMyTreasureType().getName();

			String mensajeConInfo = "Name:"+this.myAgent.getLocalName()+";";
			mensajeConInfo += "Position:"+myPosition+";";
			mensajeConInfo += "TreasureType:"+treasureType+";";


			Set<Couple<Observation,Integer>> expertises = agente.getMyExpertise();
			for (Couple<Observation,Integer> expertise: expertises) {
				String property = expertise.getLeft().getName();
				Integer value = expertise.getRight();
				mensajeConInfo += property+":"+value+";";
			}

			List<Couple<Observation,Integer>> capacities = agente.getBackPackFreeSpace();
			for (Couple<Observation,Integer> capacity: capacities) {
				String bagType = capacity.getLeft().getName();
				Integer remainingCapacity = capacity.getRight();
				mensajeConInfo += bagType+"Bag"+":"+remainingCapacity+";";
			}

			msg.setContent(mensajeConInfo);

			for (String agentName : receivers) {
				msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
			}


			//Mandatory to use this method (it takes into account the environment to decide if someone is reachable or not)
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
		}
	}
}