package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;

import jade.core.AID;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.util.Random;

import static eu.su.mas.dedaleEtu.princ.Principal.STEPS;
import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

public class ExplorationBehaviour extends SimpleBehaviour {

    private static final long serialVersionUID = 8567689731496787661L;
    private boolean finished = false;
    private MapRepresentation myMap;
    private int exit;

    public ExplorationBehaviour(final AbstractDedaleAgent myagent) {
        super(myagent);
        this.myMap=((ExplorerAgent) this.myAgent).getMyMap();
    }

    @Override
    public void action() {
        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        exit = 2;

        if(this.myMap == null) {
            this.myMap = new MapRepresentation();
            ((ExplorerAgent)this.myAgent).setStartPosition(((AbstractDedaleAgent)(this.myAgent)).getCurrentPosition());
        }

        // 0) Retrieve the current position
        String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

        if (myPosition!=null){
            //let's check whether we have an agent nearby
            MessageTemplate msgTemplate_position = MessageTemplate.and(
                    MessageTemplate.MatchProtocol("SING"),
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM) );
            List<ACLMessage> msgSingList = this.myAgent.receive(msgTemplate_position,1000);
            if (msgSingList != null) {
                ACLMessage msgSing = msgSingList.get(0);
                Couple<String, HashMap<String, String>> AgReceived=null;
                try {
                    AgReceived = (Couple<String, HashMap<String, String>>)msgSing.getContentObject();
                } catch (UnreadableException e) {
                    e.printStackTrace();
                }
                String nameAgent = AgReceived.getLeft();
                HashMap<String, String> Info = AgReceived.getRight();
                if (nameAgent.contains("coord")) { // Always add the coordinator
                    ((ExplorerAgent) this.myAgent).addAssignedAgents(nameAgent, Info.get("Position"));
                }
                // Only add the facilitator if not already in the shared map
                if (nameAgent.contains("facilitator") && !this.myMap.alreadyInAgentsInfo(nameAgent)){
                    ((ExplorerAgent) this.myAgent).addAssignedAgents(nameAgent, Info.get("Position"));
                }
                this.myMap.addAgentFound(nameAgent, Info);

                //We tell the added agent to stop singing
                String agentSendName = msgSing.getSender().getLocalName();
                ACLMessage silence_msg = new ACLMessage(ACLMessage.INFORM);
                silence_msg.setSender(this.myAgent.getAID());
                silence_msg.setProtocol("STOP-SINGING");
                silence_msg.addReceiver(new AID(agentSendName,AID.ISLOCALNAME));
                try {
                    silence_msg.setContentObject(false);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                ((AbstractDedaleAgent) this.myAgent).sendMessage(silence_msg);
                System.out.println("I am "+this.myAgent.getLocalName()+" and I found "+nameAgent);
            }

            //List of observable from the agent's current position
            List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

            //1) remove the current node from openlist and add it to closedNodes.
            this.myMap.addNode(myPosition, MapAttribute.closed);

            //2) get the surrounding nodes and, if not in closedNodes, add them to open nodes.
            String nextNode=null;

            for (Couple<String, List<Couple<Observation, Integer>>> value : lobs) {
                String nodeId = value.getLeft();
                boolean isNewNode = this.myMap.addNewNode(nodeId);
                //the node may exist, but not necessarily the edge
                if (!myPosition.equals(nodeId)) {
                    this.myMap.addEdge(myPosition, nodeId);
                    if (nextNode == null && isNewNode) nextNode = nodeId;
                }

                ArrayList<Couple<Observation, Integer>> treasureInfo = new ArrayList<>(value.getRight());

                if (treasureInfo.size() > 0) {
                    Observation type = null;
                    boolean opened = false;
                    int capacity = 0;
                    for (Couple<Observation, Integer> c : treasureInfo) {
                        String observationString = c.getLeft().toString();
                        if ("Diamond".equals(observationString)) {
                            type = Observation.DIAMOND;
                            capacity = c.getRight();
                        }
                        else if ("Gold".equals(observationString)) {
                            type = Observation.GOLD;
                            capacity = c.getRight();
                        }
                        if ("LockIsOpen".equals(c.getLeft().toString())){
                            opened = c.getRight() == 1;
                        }
                    }
                    if (!opened) {

                        int open = ((ExplorerAgent) this.myAgent).openLock(type) ? 1 : 0;
                        if (open == 1){
                            System.out.println(this.myAgent.getLocalName()+": Opened treasure "+type+" "+capacity);
                        }
                        for (int i = 0; i < treasureInfo.size(); ++i) {
                            if ("LockIsOpen".equals(treasureInfo.get(i).getLeft().toString())) {
                                treasureInfo.set(i, new Couple<>(treasureInfo.get(i).getLeft(), open));
                            }
                        }
                    }
                    this.myMap.addIfTreasures(nodeId, treasureInfo);
                }

            }

            ArrayList<String> route;
            //3) while openNodes is not empty, continues.
            if (!this.myMap.hasOpenNode()){
                exit = 3;

                Random r =  new Random();
                int waitTime = (1+r.nextInt(10))*1000;
                try {
                    this.myAgent.doWait(waitTime);
                    System.out.println(this.myAgent.getLocalName()+": Waiting "+waitTime+" ms");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(this.myAgent.getLocalName()+": Exploration successufully done.");

            }else{
                //4) select next move.
                //4.1 If there exist one open node directly reachable, go for it,
                //	 otherwise choose one from the openNode list, compute the shortestPath and go for it
                if (nextNode==null){
                    //no directly accessible openNode
                    //chose one, compute the path and take the first step.
                    route = new ArrayList<>(this.myMap.getShortestPathToClosestOpenNode(myPosition));
                    nextNode = route.get(0);
                    ((ExplorerAgent)this.myAgent).setMyRoute(route);
                }
                if(!((AbstractDedaleAgent)this.myAgent).moveTo(nextNode)) {
                    ((ExplorerAgent)this.myAgent).setStuck(((ExplorerAgent)this.myAgent).getStuck()+1);
                    exit = 1;
                }
                else {
                    ((ExplorerAgent)this.myAgent).setStuck(0);
                    ++STEPS;
                }
            }
            finished = true;
        }
        ((ExplorerAgent)this.myAgent).setMyMap(this.myMap);
    }

    public int onEnd()
    {
        return exit;
    }

    @Override
    public boolean done()
    {
        return finished;
    }
}
