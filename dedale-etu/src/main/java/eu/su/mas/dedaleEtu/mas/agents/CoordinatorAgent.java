package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;

import eu.su.mas.dedaleEtu.mas.behaviours.*;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

public class CoordinatorAgent extends AbstractDedaleAgent {

    private MapRepresentation myMap;
    private int closedTreasureIndex = 0;
    private int treasureGoldIndex = 0;
    private int treasureDiamondIndex = 0;
    private String startPosition = null;
    private ArrayList<String> myRoute;
    private ArrayList<String> inverseRoute;
    private ArrayList<String> agentsSentToTreasures; // Explorer agents already sent to treasures

    private Boolean goldAgent;
    private Boolean diamondAgent;
    private boolean singingStatus = true;
    private boolean explorationPhase = true;
    private boolean hello = false;



    protected void setup(){

        super.setup();
        this.myRoute = new ArrayList<>();
        this.inverseRoute = new ArrayList<>();
        this.goldAgent = false;
        this.diamondAgent = false;
        this.agentsSentToTreasures = new ArrayList<>();

        List<Behaviour> lb= new ArrayList<>();
        FSMBehaviour fsm = new FSMBehaviour();

        fsm.registerFirstState(new WaitForActionBehaviour(this,"Coord"), "singAndWait");
        fsm.registerState(new PassThroughReceiveBehaviour(fsm, this, "Coord"), "passReceive");
        fsm.registerState(new ShareInfoBehaviour(fsm, this, "Coord"), "shareInfo");
        fsm.registerState(new MoveToNodeBehaviour( this, "Coord"), "moveToNode");
        fsm.registerState(new OrderActionBehaviour(this), "orderAction");
        fsm.registerState(new PassThroughSendBehaviour(fsm, this, "Coord"), "passSend");

        fsm.registerDefaultTransition("singAndWait", "passReceive");

        fsm.registerDefaultTransition("passReceive", "moveToNode");

        fsm.registerDefaultTransition("moveToNode", "moveToNode");
        fsm.registerTransition("moveToNode", "shareInfo", 2);
        fsm.registerTransition("moveToNode", "orderAction", 3);
        fsm.registerTransition("moveToNode", "passSend", 4);
        fsm.registerDefaultTransition("passSend", "passReceive");

        fsm.registerDefaultTransition("shareInfo", "singAndWait");

        fsm.registerDefaultTransition("orderAction", "passReceive");

        lb.add(fsm);
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("The agent "+this.getLocalName()+ " is started");
    }

    public ArrayList<String> getReceivers() {
        String[] receivers = {"explorer1","explorer2","explorer3",
                              "collectorGold1","collectorGold2",
                              "collectorDiamond1","collectorDiamond2", "facilitatorDiamond", "facilitatorGold"};
        return new ArrayList<>(Arrays.asList(receivers));
    }


    public MapRepresentation getMyMap()
    {
        return myMap;
    }

    public ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getTreasures(){
        return this.myMap.getAllTreasures();
    }

    public void setMyMap(MapRepresentation myMap) {
        this.myMap = myMap;
    }

    public ArrayList<String> getMyRoute()
    {
        return myRoute;
    }

    public void setMyRoute(ArrayList<String> myRoute)
    {
        this.myRoute = myRoute;
    }

    public void setStartPosition(String startPosition)
    {
        this.startPosition = startPosition;
    }

    public String getStartPosition()
    {
        return this.startPosition;
    }

    public ArrayList<String> getInverseRoute() {
        return inverseRoute;
    }

    public void addToMyRoute(String node) {
        this.myRoute.add(0,node);
    }

    public void addToInverseRoute(String node) {
        this.inverseRoute.add(0, node);
    }

    public int getClosedTreasureIndex()
    {
        return closedTreasureIndex;
    }

    public void increaseClosedTreasureIndex(){
        closedTreasureIndex += 1;
    }

    public int getTreasureIndex(boolean gold){
        return gold ? treasureGoldIndex:treasureDiamondIndex;
    }

    public void increaseTreasureIndex(boolean gold){
        if (gold) treasureGoldIndex += 1;
        else treasureDiamondIndex += 1;
    }

    public ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> getClosedTreasures() {
        return this.myMap.getClosedTreasures();
    }

    public Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> getIthTreasure(int index, boolean gold)
    {
        if (gold) return this.myMap.getGoldTreasures().get(index);
        else return this.myMap.getDiamondTreasures().get(index);
    }

    public void setIthTreasure(int index, Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure, boolean gold)
    {
        if (gold) this.myMap.setIthGoldTreasure(index, treasure);
        else this.myMap.setIthDiamondTreasure(index, treasure);
    }

    public Boolean getGoldAgent() {
        return goldAgent;
    }

    public void setGoldAgent(Boolean goldAgent) {
        this.goldAgent = goldAgent;
    }

    public Boolean getDiamondAgent() {
        return diamondAgent;
    }

    public void setDiamondAgent(Boolean diamondAgent) {
        this.diamondAgent = diamondAgent;
    }

    public void addSendTreasure(String agentName) {
        this.agentsSentToTreasures.add(agentName);
    }

    public boolean alreadySendTreasure(String agentName) {
        return this.agentsSentToTreasures.contains(agentName);
    }
    public int getAgentsSentToTreasuresSize() {
        return this.agentsSentToTreasures.size();
    }

    public boolean getSingingStatus() {
        return this.singingStatus;
    }

    public void setSingingStatus(boolean status) {
        this.singingStatus = status;
    }

    public boolean getExplorationPhase() {
        return explorationPhase;
    }
    public void setExplorationPhase(boolean explorationPhase) {
        this.explorationPhase = explorationPhase;
    }

    public ArrayList<Couple<String,String>> getTreasuresAssignment(){
        ArrayList<Couple<String,String>> assignment = new ArrayList<>();
        String[] explorersNames = {"explorer1","explorer2","explorer3"};
        ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> treasures = this.myMap.getClosedTreasures();
        for (int i = 0; i < explorersNames.length; ++i) {
            String explorerName = explorersNames[i];
            String treasure = null;
            if (i+1 <= treasures.size()) treasure = treasures.get(i).getFirst();
            Couple<String,String> ass = new Couple<>(explorerName, treasure);
            assignment.add(ass);
        }
        return assignment;
    }

    public boolean isHello() {
        return hello;
    }

    public void setHello(boolean hello) {
        this.hello = hello;
    }
}