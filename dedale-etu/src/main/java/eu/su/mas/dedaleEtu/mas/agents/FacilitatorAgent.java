package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;

import eu.su.mas.dedaleEtu.mas.behaviours.*;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

public class FacilitatorAgent extends AbstractDedaleAgent {
    private String startPosition = null;
    private ArrayList<String> myRoute;
    private ArrayList<String> inverseRoute;
    private Tuple4<ArrayList<String>, ArrayList<String>,ArrayList<String>,Integer> myAction; // Route to the treasure and to the tanker + to initial position
    private String type;
    private boolean singingStatus = true;
    private boolean explorationPhase = true;
    private String collectorName = "";
    private int stuck = 0;

    protected void setup(){

        super.setup();
        this.myRoute = new ArrayList<>();
        this.inverseRoute = new ArrayList<>();
        this.myAction = null;
        this.type = Objects.equals(this.getLocalName(), "facilitatorGold") ? "gold" : "diamond";

        List<Behaviour> lb= new ArrayList<>();
        FSMBehaviour fsm = new FSMBehaviour();

        fsm.registerFirstState(new WaitForActionBehaviour(this,"Fac"), "singAndWait");
        fsm.registerState(new PassThroughReceiveBehaviour(fsm, this, "Fac"), "passReceive");
        fsm.registerState(new PassThroughSendBehaviour(fsm, this, "Fac"), "passSend");
        fsm.registerState(new ReceiveOrderBehaviour(this), "receiveOrder");
        fsm.registerState(new OrderCollectorBehaviour(this), "orderCollector");
        fsm.registerState(new MoveToNodeBehaviour( this, "Fac"), "moveToNode");

        fsm.registerDefaultTransition("singAndWait", "passReceive");

        fsm.registerDefaultTransition("passReceive", "moveToNode");

        fsm.registerDefaultTransition("passSend", "passReceive");

        fsm.registerDefaultTransition("moveToNode", "moveToNode");
        fsm.registerTransition("moveToNode", "orderCollector", 2);
        fsm.registerTransition("moveToNode", "receiveOrder", 3);
        fsm.registerTransition("moveToNode", "passSend", 4);
        fsm.registerTransition("moveToNode", "singAndWait", 5);

        fsm.registerDefaultTransition("orderCollector", "passReceive");

        fsm.registerDefaultTransition("receiveOrder", "passReceive");

        lb.add(fsm);
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("The agent "+this.getLocalName()+ " is started");
    }

    public ArrayList<String> getReceivers() {
        String[] receivers = {"explorer1","explorer2","explorer3",
                              "collectorGold1","collectorGold2",
                              "collectorDiamond1","collectorDiamond2",
                              "coordinator"};
        return new ArrayList<>(Arrays.asList(receivers));
    }

    public ArrayList<String> getMyRoute()
    {
        return myRoute;
    }

    public void setMyRoute(ArrayList<String> myRoute)
    {
        this.myRoute = myRoute;
    }

    public void setStartPosition(String startPosition)
    {
        this.startPosition = startPosition;
    }

    public String getStartPosition()
    {
        return this.startPosition;
    }

    public ArrayList<String> getInverseRoute() {
        return inverseRoute;
    }

    public void addToMyRoute(String node) {
        this.myRoute.add(0,node);
    }

    public void addToInverseRoute(String node) {
        this.inverseRoute.add(0, node);
    }
    public void setInverseRoute(ArrayList<String> inverseRoute) {
        this.inverseRoute=inverseRoute;
    }
    public String getType() {
        return type;
    }

    public Tuple4<ArrayList<String>, ArrayList<String>,ArrayList<String>,Integer> getMyAction() {
        return myAction;
    }

    public void setMyAction(Tuple4<ArrayList<String>, ArrayList<String>,ArrayList<String>,Integer> myAction) {
        this.myAction = myAction;
    }

    public boolean getSingingStatus() {
        return this.singingStatus;
    }

    public void setSingingStatus(boolean status) {
        this.singingStatus = status;
    }

    public boolean getExplorationPhase() {
        return this.explorationPhase;
    }

    public void setExplorationPhase(boolean explorationPhase) {
        this.explorationPhase = explorationPhase;
    }

    public String getCollectorName() {
        return collectorName;
    }

    public void setCollectorName(String collectorName) {
        this.collectorName = collectorName;
    }

    public int getStuck() {
        return stuck;
    }

    public void setStuck(int stuck) {
        this.stuck = stuck;
    }
}