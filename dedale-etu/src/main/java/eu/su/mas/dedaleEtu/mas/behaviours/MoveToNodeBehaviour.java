package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.*;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;

import eu.su.mas.dedaleEtu.mas.agents.*;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.behaviours.ShareInfoBehaviour;


import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import static eu.su.mas.dedaleEtu.princ.Principal.STEPS;
import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

public class MoveToNodeBehaviour extends OneShotBehaviour {

    private ArrayList<String> route;
    private String myAction;
    private String typeOfAgent;
    private int exit;

    public MoveToNodeBehaviour(final AbstractDedaleAgent myagent, String typeOfAgent)
    {
        super(myagent);
        this.typeOfAgent = typeOfAgent;
    }

    @Override
    public void action() {

        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Objects.equals(typeOfAgent, "Explorer")) {
            this.route = ((ExplorerAgent)this.myAgent).getMyRoute();
            this.myAction = ((ExplorerAgent)this.myAgent).getMyAction();
        } else if (Objects.equals(typeOfAgent, "Collector")){ // Collector
            this.route = ((CollectorAgent)this.myAgent).getMyRoute();
            this.myAction = ((CollectorAgent)this.myAgent).getMyAction();
        } else if (Objects.equals(typeOfAgent, "Coord")){ // Coordinator
            this.route = ((CoordinatorAgent)this.myAgent).getMyRoute();
        }else if (Objects.equals(typeOfAgent, "Fac")){ // Coordinator
            this.route = ((FacilitatorAgent)this.myAgent).getMyRoute();
        }else if (Objects.equals(typeOfAgent, "Tanker")){ // Coordinator
            this.route = ((TankerAgent)this.myAgent).getMyRoute();
        }


        if (route.isEmpty()) {
            //we get to our destiny

            if (Objects.equals(this.typeOfAgent, "Collector")){
                exit = 3;
                if (this.myAction != null) exit = 2;
            }
            else if (Objects.equals(this.typeOfAgent, "Explorer")) {
                if (this.myAction == null){
                    exit = 5;
                }
                else if (this.myAction.equals("shareInfoCoord") || this.myAction.equals("shareInfoFac")) exit = 3;
                else exit = 2;
            }
            else if (Objects.equals(this.typeOfAgent, "Coord")) {
                if (((CoordinatorAgent)this.myAgent).getExplorationPhase()) {
                    exit = 2;
                }
                else exit = 3; // Share info
            }
            else if (Objects.equals(this.typeOfAgent, "Fac")) {
                if (((FacilitatorAgent)this.myAgent).getExplorationPhase()) {
                    exit = 5;
                }
                else if (((FacilitatorAgent)this.myAgent).getMyAction() == null) {
                   exit = 3;
                }
                else exit = 2;
            }
            else if (Objects.equals(this.typeOfAgent, "Tanker")) {
                exit = 2;
            }
            return;
        }
        if (!((AbstractDedaleAgent)this.myAgent).moveTo(this.route.get(0))) {
            // Pass though
            if (this.typeOfAgent.equals("Explorer")) {
                ((ExplorerAgent)this.myAgent).setStuck(((ExplorerAgent)this.myAgent).getStuck()+1);
            } else if (this.typeOfAgent.equals("Collector")) {
                ((CollectorAgent)this.myAgent).setStuck(((CollectorAgent)this.myAgent).getStuck()+1);
            } else if (this.typeOfAgent.equals("Fac")) {
                ((FacilitatorAgent)this.myAgent).setStuck(((FacilitatorAgent)this.myAgent).getStuck()+1);
            }
            exit = 4;
            return;
        } else ++STEPS;

        if (this.typeOfAgent.equals("Explorer")) {
            ((ExplorerAgent)this.myAgent).setStuck(0);
        } else if (this.typeOfAgent.equals("Collector")) {
            ((CollectorAgent)this.myAgent).setStuck(0);
        } else if (this.typeOfAgent.equals("Fac")) {
            ((FacilitatorAgent)this.myAgent).setStuck(0);
        }

        route.remove(0);
        exit = 1;
    }

    public int onEnd()
    {
        return exit;
    }

}
