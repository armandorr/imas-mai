package eu.su.mas.dedaleEtu.mas.behaviours;

import java.util.ArrayList;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.behaviours.OneShotBehaviour;

public class ShareCoordBehaviour extends OneShotBehaviour {

    private int exit;

    public ShareCoordBehaviour(final AbstractDedaleAgent myagent)
    {
        super(myagent);
    }

    @Override
    public void action() {
        MapRepresentation myMap = ((ExplorerAgent) this.myAgent).getMyMap();
        List<Couple<String, String>> pendingCoordinatorsPos = ((ExplorerAgent) this.myAgent).getAssignedAgents();
        System.out.println("pendingCoordinatorsPos: " +this.myAgent.getLocalName()+ " "+ pendingCoordinatorsPos);
        if (pendingCoordinatorsPos != null) {
            if (!pendingCoordinatorsPos.isEmpty()) {
                Couple<String, String> name_pos = pendingCoordinatorsPos.get(0);
                if (name_pos.getLeft().contains("coord")) ((ExplorerAgent) this.myAgent).setMyAction("shareInfoCoord");
                else {
                    ((ExplorerAgent) this.myAgent).setMyAction("shareInfoFac");
                    ((ExplorerAgent) this.myAgent).setFacilitatorToBeInformed(name_pos);
                }
                exit = 3;

                String myPosition = ((AbstractDedaleAgent) this.myAgent).getCurrentPosition();
                String pos = name_pos.getRight();
                System.out.println(this.myAgent.getLocalName()+": Going to share info with "+name_pos.getLeft()+" at "+ pos);
                ArrayList<String> route = new ArrayList<>(myMap.getShortestPath(myPosition, pos));
                if (!route.isEmpty()) route.remove(route.size() - 1);
                ((ExplorerAgent) this.myAgent).setMyRoute(route);
                ((ExplorerAgent) this.myAgent).eraseFirstPendingInformationPos();
                return;
            }
        }
        System.out.println(this.myAgent.getLocalName()+": All my coordinators know all his information");
        exit = 1;
        ((ExplorerAgent) this.myAgent).setMyAction(null);
    }

    public int onEnd()
    {
        return exit;
    }

}
