package eu.su.mas.dedaleEtu.mas.behaviours;

import java.io.IOException;
import java.util.*;

import dataStructures.serializableGraph.SerializableSimpleGraph;

import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.CoordinatorAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.FSMBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;

import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)  	
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class ShareInfoBehaviour extends SimpleBehaviour{
	
	private MapRepresentation myMap;
	private List<String> receivers;
	private String typeOfAgent;

	private String myAction = null;

	private boolean finished = false;
	private final FSMBehaviour fsm;

	/**
	 * The agent periodically share its map.
	 * It blindly tries to send all its graph to its friend(s)  	
	 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

	 * @param a the agent
	 * @param period the periodicity of the behaviour (in ms)
	 * @param mymap (the map to share)
	 * @param receivers the list of agents to send the map to
	 */
	public ShareInfoBehaviour(FSMBehaviour fsm, Agent myagent, String typeOfAgent) {
		super(myagent);
		this.typeOfAgent = typeOfAgent;
		this.fsm = fsm;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -568863390879327961L;
	int exit;
	@Override
	public void action() {
		exit = this.fsm.getLastExitValue();
		finished = true;
		try {
			this.myAgent.doWait(TICKS);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (Objects.equals(this.typeOfAgent, "Explorer")) {
			this.receivers = ((ExplorerAgent) this.myAgent).getReceivers();
			this.myMap = ((ExplorerAgent) this.myAgent).getMyMap();
			this.myAction = ((ExplorerAgent)this.myAgent).getMyAction();
		}
		else if (Objects.equals(this.typeOfAgent, "Coord")) {
			this.receivers = ((CoordinatorAgent) this.myAgent).getReceivers();
			this.myMap = ((CoordinatorAgent) this.myAgent).getMyMap();
		}

		// Send and receive map
		this.sendReceiveMap();

		// Send and receive treasures
		this.sendReceiveTreasures();

		// Send and receive agentInfo in map
		this.sendReceiveAgentsInfo();


		// ONLY coordinator
		if (Objects.equals(this.typeOfAgent, "Coord")) {
			this.sendTreasureToOpen();
			this.receiveConfirmationTreasureToOpen();
		}

		// Inform coordinators
		if (this.fsm.getLastExitValue() == 3 && Objects.equals(this.typeOfAgent, "Explorer")) {
			if (this.myAction.equals("shareInfoCoord")) {
				//Si llegamos hasta aqui, sabemos que estamos informando a un coordinator, y el coordinator nos tiene que dar la posición del tesoro

				ACLMessage msg_ask = new ACLMessage(ACLMessage.INFORM);
				msg_ask.setProtocol("ASK-FOR-TREASURE");
				msg_ask.setSender(this.myAgent.getAID());
				msg_ask.addReceiver(new AID("coordinator", AID.ISLOCALNAME));
				msg_ask.setContent("Give me treasure to open");
				((AbstractDedaleAgent) this.myAgent).sendMessage(msg_ask);

				// we wait for the treasure
				try {
					this.myAgent.doWait(3000);
				} catch (Exception e) {
					e.printStackTrace();
				}

				MessageTemplate msgTemplateLocation = MessageTemplate.and(
						MessageTemplate.MatchProtocol("GIVING-TREASURE-TO-OPEN"),
						MessageTemplate.MatchPerformative(ACLMessage.INFORM));

				ACLMessage location_message = this.myAgent.receive(msgTemplateLocation);
				String treasure = ((ExplorerAgent)this.myAgent).getTreasureToOpenPosition();
				if (location_message != null && treasure == null) {
					System.out.println(this.myAgent.getLocalName() + ": I have assigned the treasure in position "+location_message.getContent());
					((ExplorerAgent) this.myAgent).setTreasureToOpenPosition(location_message.getContent());

					// Confirmation
					ACLMessage confirmationMsg = new ACLMessage(ACLMessage.INFORM);
					confirmationMsg.setProtocol("CONFIRM-TREASURE");
					confirmationMsg.setSender(this.myAgent.getAID());
					confirmationMsg.addReceiver(new AID(location_message.getSender().getLocalName(), AID.ISLOCALNAME));
					try {
						confirmationMsg.setContentObject("Recieved");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					((AbstractDedaleAgent) this.myAgent).sendMessage(confirmationMsg);
				} else exit = 4;

			} else if (this.myAction.equals("shareInfoFac")) {
				//Si llegamos hasta aqui, sabemos que estamos informando a un facilitator
				ACLMessage msgfacilitatorRoute = new ACLMessage(ACLMessage.INFORM);
				msgfacilitatorRoute.setProtocol("SENDING-ROUTE");

				msgfacilitatorRoute.setSender(this.myAgent.getAID());
				Couple<String, String> facilitator = ((ExplorerAgent) this.myAgent).getFacilitatorToBeInformed();
				msgfacilitatorRoute.addReceiver(new AID(facilitator.getLeft(), AID.ISLOCALNAME));

				String coordinator_pos = this.myMap.getCoordinatorPosition();
				ArrayList<String> route = new ArrayList<>(myMap.getShortestPath(facilitator.getRight(), coordinator_pos));
				if (!route.isEmpty()) route.remove(route.size() - 1);
				if(Objects.equals(facilitator.getLeft(), "facilitatorDiamond") && (!route.isEmpty())) route.remove(route.size() - 1);

				try {
					msgfacilitatorRoute.setContentObject(route);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				((AbstractDedaleAgent) this.myAgent).sendMessage(msgfacilitatorRoute);

				// we wait for the confirmation message
				try {
					this.myAgent.doWait(3000);
				} catch (Exception e) {
					e.printStackTrace();
				}

				MessageTemplate msgTemplateLocation = MessageTemplate.and(
						MessageTemplate.MatchProtocol("SENDING-ROUTE"),
						MessageTemplate.MatchPerformative(ACLMessage.INFORM));

				ACLMessage confirmation_message = this.myAgent.receive(msgTemplateLocation);
				if (confirmation_message != null) {
					System.out.println(this.myAgent.getLocalName() + ": I have recieved confirmation from facilitator");
				} else exit = 4;

			}
		}
	}

	private void sendReceiveMap() {
		// Send
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("SHARE-TOPO");
		msg.setSender(this.myAgent.getAID());
		for (String agentName : receivers) {
			if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
				msg.addReceiver(new AID(agentName, AID.ISLOCALNAME));
			}
		}
		SerializableSimpleGraph<String, MapAttribute> sg = this.myMap.getSerializableGraph();
		try {
			msg.setContentObject(sg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg);

		//Receive
		MessageTemplate msgTemplate = MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TOPO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		List<ACLMessage> msgRouteList = this.myAgent.receive(msgTemplate,1000);
		if (msgRouteList!=null) {
			ACLMessage msgReceived = msgRouteList.get(0);
			SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
			try {
				sgreceived = (SerializableSimpleGraph<String, MapAttribute>) msgReceived.getContentObject();
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
			this.myMap.mergeMap(sgreceived);
		}
	}

	private void sendReceiveTreasures() {
		// Send treasures
		ACLMessage msg_treassure = new ACLMessage(ACLMessage.INFORM);
		msg_treassure.setProtocol("SHARE-TREASURES");
		msg_treassure.setSender(this.myAgent.getAID());
		for (String agentName : receivers) {
			if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
				msg_treassure.addReceiver(new AID(agentName, AID.ISLOCALNAME));
			}
		}
		ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> treasures = this.myMap.getAllTreasures();
		try {
			msg_treassure.setContentObject(treasures);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_treassure);

		//Receive treasures
		MessageTemplate msgTemplateTr = MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-TREASURES"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		List<ACLMessage> msgTreasureList = this.myAgent.receive(msgTemplateTr,1000);
		if (msgTreasureList!=null) {
			ACLMessage msgTreasure = msgTreasureList.get(0);
			ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> trReceived=null;
			try {
				trReceived = (ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>>) msgTreasure.getContentObject();
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
			this.myMap.mergeTreasures(trReceived);
		}
	}

	private void sendReceiveAgentsInfo() {
		// Send agents info
		ACLMessage msg_agentsinfo = new ACLMessage(ACLMessage.INFORM);
		msg_agentsinfo.setProtocol("SHARE-AGENTSINFO");
		msg_agentsinfo.setSender(this.myAgent.getAID());
		for (String agentName : receivers) {
			if (!Objects.equals(agentName, this.myAgent.getLocalName())) {
				msg_agentsinfo.addReceiver(new AID(agentName, AID.ISLOCALNAME));
			}
		}
		HashMap<String, HashMap<String, String>> agentsinfo = this.myMap.getAgentsFound();
		try {
			msg_agentsinfo.setContentObject(agentsinfo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_agentsinfo);

		//Receive agents info
		MessageTemplate msgTemplateAg = MessageTemplate.and(
				MessageTemplate.MatchProtocol("SHARE-AGENTSINFO"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		List<ACLMessage> msgAgentsList = this.myAgent.receive(msgTemplateAg,1000);
		if (msgAgentsList!=null) {
			ACLMessage msgAgents = msgAgentsList.get(0);
			HashMap<String, HashMap<String, String>> AgReceived=null;
			try {
				AgReceived = (HashMap<String, HashMap<String, String>>) msgAgents.getContentObject();
			} catch (UnreadableException e) {
				e.printStackTrace();
			}
			if (Objects.equals(this.typeOfAgent, "Explorer")) {
				((ExplorerAgent) this.myAgent).addAssignedAgentsFromHashMap(AgReceived);
			}
			this.myMap.mergeAgentsInfo(AgReceived);
		}
	}

	private void sendTreasureToOpen() {
		MessageTemplate msgTemplateAskingLocation = MessageTemplate.and(
				MessageTemplate.MatchProtocol("ASK-FOR-TREASURE"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		List<ACLMessage> asking_for_locationList = this.myAgent.receive(msgTemplateAskingLocation,1000);

		if (asking_for_locationList != null) {
			ACLMessage asking_for_location = asking_for_locationList.get(0);
			String agentSendName = asking_for_location.getSender().getLocalName();

			ArrayList<Couple<String, String>> closedTreasures
					= ((CoordinatorAgent) this.myAgent).getTreasuresAssignment();

			for (Couple<String, String> assig : closedTreasures){
				if (Objects.equals(agentSendName, assig.getLeft())){
					String treasureToOpen = assig.getRight();
					System.out.println("I give the treasure "+treasureToOpen+" to "+agentSendName);

					ACLMessage msg_answer = new ACLMessage(ACLMessage.INFORM);
					msg_answer.setProtocol("GIVING-TREASURE-TO-OPEN");
					msg_answer.setSender(this.myAgent.getAID());
					msg_answer.addReceiver(new AID(agentSendName, AID.ISLOCALNAME));
					msg_answer.setContent(treasureToOpen);
					((AbstractDedaleAgent) this.myAgent).sendMessage(msg_answer);
				}
			}
		}
	}

	private void receiveConfirmationTreasureToOpen() {
		MessageTemplate msgTemplateLocation = MessageTemplate.and(
				MessageTemplate.MatchProtocol("CONFIRM-TREASURE"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));

		ACLMessage confirmation_message = this.myAgent.receive(msgTemplateLocation);
		if (confirmation_message != null) {
			String agentSendName = confirmation_message.getSender().getLocalName();
			System.out.println(this.myAgent.getLocalName() + ": I have recieved confirmation from explorers");
			((CoordinatorAgent) this.myAgent).addSendTreasure(agentSendName);
			if (((CoordinatorAgent) this.myAgent).getAgentsSentToTreasuresSize() >= 3)
				((CoordinatorAgent) this.myAgent).setExplorationPhase(false);
		} else exit = 4;
	}
	//finished = true;

	public int onEnd()
	{
		return exit;
	}

	@Override
	public boolean done() {
		return true;
	}

}
