package eu.su.mas.dedaleEtu.mas.behaviours;
import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.agents.CoordinatorAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;

import eu.su.mas.dedaleEtu.mas.agents.FacilitatorAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;


public class OrderCollectorBehaviour extends OneShotBehaviour {
    private final String[] receivers = {"collectorGold1","collectorGold2","collectorDiamond1","collectorDiamond2"};

    public OrderCollectorBehaviour(Agent a)
    {
        super(a);
    }

    @Override
    public void action() {
        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Send route
        ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
        msg_route.setProtocol("DO-ACTION");
        msg_route.setSender(this.myAgent.getAID());
        msg_route.addReceiver(new AID(((FacilitatorAgent) this.myAgent).getCollectorName(), AID.ISLOCALNAME));
        Tuple4<ArrayList<String>, ArrayList<String>, ArrayList<String>,Integer> routeForCollector = ((FacilitatorAgent) this.myAgent).getMyAction();
        String type = ((FacilitatorAgent) this.myAgent).getType();
        Tuple4<ArrayList<String>, Couple<String,Integer>, ArrayList<String>, ArrayList<String>> msg = new Tuple4<>(routeForCollector.get_1(), new Couple<>(type,routeForCollector.get_4()), routeForCollector.get_2(), routeForCollector.get_3());
        try {
            msg_route.setContentObject(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("I'm the coordinator and I send an acion to the facilitator");
        ((AbstractDedaleAgent)this.myAgent).sendMessage(msg_route);

        try {
            this.myAgent.doWait(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //check if collector has received it
        MessageTemplate msgTemplate = MessageTemplate.and(
                MessageTemplate.MatchProtocol("COLLECTOR-OK"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM));

        List<ACLMessage> msgRouteList = this.myAgent.receive(msgTemplate,1000);
        if (msgRouteList != null) {
            ((FacilitatorAgent) this.myAgent).setMyAction(null);
            ArrayList<String> inverseRoute = ((FacilitatorAgent) this.myAgent).getInverseRoute();
            ((FacilitatorAgent) this.myAgent).setMyRoute(inverseRoute);
        }
    }

    public int onEnd()
    {
        return 3;
    }
}
