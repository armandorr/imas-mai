package eu.su.mas.dedaleEtu.mas.behaviours;
import dataStructures.tuple.Couple;
import dataStructures.tuple.Tuple3;
import dataStructures.tuple.Tuple4;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.CollectorAgent;
import eu.su.mas.dedaleEtu.mas.agents.CoordinatorAgent;
import eu.su.mas.dedaleEtu.mas.agents.ExplorerAgent;

import eu.su.mas.dedaleEtu.mas.knowledge.AgentsInfo;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Treasures;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static eu.su.mas.dedaleEtu.princ.Principal.STEPS;
import static eu.su.mas.dedaleEtu.princ.Principal.TICKS;

/**
 * The agent periodically share its map.
 * It blindly tries to send all its graph to its friend(s)
 * If it was written properly, this sharing action would NOT be in a ticker behaviour and only a subgraph would be shared.

 * @author hc
 *
 */
public class OrderActionBehaviour extends OneShotBehaviour {
    private final String[] receivers = {"facilitatorGold","facilitatorDiamond"};
    private MapRepresentation myMap;

    public OrderActionBehaviour(Agent a)
    {
        super(a);
    }

    @Override
    public void action() {
        this.myMap=((CoordinatorAgent) this.myAgent).getMyMap();
        ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> diamondTreasures = this.myMap.getDiamondTreasures();
        ArrayList<Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>>> goldTreasures = this.myMap.getGoldTreasures();
        AgentsInfo agentsInfo = this.myMap.getAgentsinfo();
        try {
            this.myAgent.doWait(TICKS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        MessageTemplate msgTemplate = MessageTemplate.and(
                MessageTemplate.MatchProtocol("FACILITATOR-HELLO"),
                MessageTemplate.MatchPerformative(ACLMessage.INFORM));

        List<ACLMessage> msgInfoList = this.myAgent.receive(msgTemplate,1000);

        if (msgInfoList != null) {
            String agentPosition;
            String collectorName = "";
            System.out.println("I'm the coordinator and I a facilitator is aking for orders");
            ArrayList<String> routeToTreasure = null;
            ArrayList<String> routeToTanker = null;
            ArrayList<String> routeToCollector = null;
            ACLMessage messageInfo = msgInfoList.get(0);
            Couple<String, String> infoReceived = null;
            int quantity = -1;
            try {
                infoReceived = (Couple<String, String>) messageInfo.getContentObject();
            } catch (UnreadableException e) {
                e.printStackTrace();
            }
            if (Objects.equals(infoReceived.getLeft(), "Diamond")) {
                if (((CoordinatorAgent) this.myAgent).getDiamondAgent()) {
                    agentPosition = agentsInfo.getAgentPosition("collectorDiamond1");
                    collectorName = "collectorDiamond1";
                }
                else {
                    agentPosition = agentsInfo.getAgentPosition("collectorDiamond2");
                    collectorName = "collectorDiamond2";
                }
                ((CoordinatorAgent) this.myAgent).setDiamondAgent(!((CoordinatorAgent) this.myAgent).getDiamondAgent());
                int index = 0;
                for (int i = 0; i < diamondTreasures.size(); ++i) {
                    if (diamondTreasures.get(i).getThird().get_1() > 0) {
                        String tresurePos = diamondTreasures.get(i).getFirst();
                        ArrayList<String> tmpRoute = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(agentPosition, tresurePos, ((CoordinatorAgent) this.myAgent).getStartPosition());
                        if (routeToTreasure == null || tmpRoute.size() < routeToTreasure.size()) {
                            routeToTreasure = new ArrayList<>(tmpRoute);  //clone necessary?
                            index = i;
                        }
                    }
                }
                if (routeToTreasure == null) {
                    ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
                    msg_route.setProtocol("ROUTE-FACILITATOR");
                    msg_route.setSender(this.myAgent.getAID());
                    msg_route.addReceiver(new AID(messageInfo.getSender().getLocalName(), AID.ISLOCALNAME));
                    try {
                        msg_route.setContentObject(null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ((AbstractDedaleAgent)this.myAgent).sendMessage(msg_route);
                    return;
                }
                Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure = diamondTreasures.get(index);
                quantity = treasure.getThird().get_1();
                Tuple4<Integer, Integer, Integer, Boolean> treasureInfo = new Tuple4<>(treasure.getThird().get_1()-50, treasure.getThird().get_2(), treasure.getThird().get_3(), treasure.getThird().get_4());
                this.myMap.setIthDiamondTreasure(index, new Tuple3<>(treasure.getFirst(), treasure.getSecond(), treasureInfo));
                ArrayList<String> routeToTanker1 = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(treasure.getFirst(), agentsInfo.getAgentPosition("tanker1"), ((CoordinatorAgent) this.myAgent).getStartPosition());
                ArrayList<String> routeToTanker2 = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(treasure.getFirst(), agentsInfo.getAgentPosition("tanker2"), ((CoordinatorAgent) this.myAgent).getStartPosition());
                routeToTanker = routeToTanker1.size()>routeToTanker2.size() ? routeToTanker2 : routeToTanker1;
                routeToCollector = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(infoReceived.getRight(), agentPosition, ((CoordinatorAgent) this.myAgent).getStartPosition());
            }
            else { //Gold
                if (((CoordinatorAgent) this.myAgent).getGoldAgent()) {
                    agentPosition = agentsInfo.getAgentPosition("collectorGold1");
                    collectorName = "collectorGold1";
                }
                else {
                    agentPosition = agentsInfo.getAgentPosition("collectorGold2");
                    collectorName = "collectorGold2";
                }
                ((CoordinatorAgent) this.myAgent).setGoldAgent(!((CoordinatorAgent) this.myAgent).getGoldAgent());
                int index = 0;
                for (int i = 0; i < goldTreasures.size(); ++i) {
                    if (goldTreasures.get(i).getThird().get_1() > 0) {
                        String tresurePos = goldTreasures.get(i).getFirst();
                        ArrayList<String> tmpRoute = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(agentPosition, tresurePos, ((CoordinatorAgent) this.myAgent).getStartPosition());
                        if (routeToTreasure == null || tmpRoute.size() < routeToTreasure.size()) {
                            routeToTreasure = new ArrayList<>(tmpRoute);
                            index = i;
                        }
                    }
                }
                if (routeToTreasure == null) {
                    ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
                    msg_route.setProtocol("ROUTE-FACILITATOR");
                    msg_route.setSender(this.myAgent.getAID());
                    msg_route.addReceiver(new AID(messageInfo.getSender().getLocalName(), AID.ISLOCALNAME));
                    try {
                        msg_route.setContentObject(null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ((AbstractDedaleAgent)this.myAgent).sendMessage(msg_route);
                    return;
                }
                Tuple3<String, String, Tuple4<Integer, Integer, Integer, Boolean>> treasure = goldTreasures.get(index);
                quantity = treasure.getThird().get_1();
                Tuple4<Integer, Integer, Integer, Boolean> treasureInfo = new Tuple4<>(treasure.getThird().get_1()-50, treasure.getThird().get_2(), treasure.getThird().get_3(), treasure.getThird().get_4());
                this.myMap.setIthGoldTreasure(index, new Tuple3<>(treasure.getFirst(), treasure.getSecond(), treasureInfo));
                ArrayList<String> routeToTanker1 = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(treasure.getFirst(), agentsInfo.getAgentPosition("tanker1"), ((CoordinatorAgent) this.myAgent).getStartPosition());
                ArrayList<String> routeToTanker2 = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(treasure.getFirst(), agentsInfo.getAgentPosition("tanker2"), ((CoordinatorAgent) this.myAgent).getStartPosition());
                routeToTanker = routeToTanker1.size()>routeToTanker2.size() ? routeToTanker2 : routeToTanker1;
                routeToCollector = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(infoReceived.getRight(), agentPosition, ((CoordinatorAgent) this.myAgent).getStartPosition());
            }
            String iniRoute = "";
            if (routeToCollector.size() > 0) routeToCollector.remove(routeToCollector.size()-1);
            if (routeToTanker.size() > 0) {
                routeToTanker.remove(routeToTanker.size()-1);
                iniRoute = routeToTanker.get(routeToTanker.size()-1);
            } else iniRoute = routeToTreasure.get(routeToTreasure.size()-1);

            ArrayList<String> routeFromTankerToInitialCollector = (ArrayList<String>) this.myMap.getShortestPathWithoutNode(iniRoute, agentPosition, ((CoordinatorAgent) this.myAgent).getStartPosition());

            ACLMessage msg_route = new ACLMessage(ACLMessage.INFORM);
            msg_route.setProtocol("ROUTE-FACILITATOR");
            msg_route.setSender(this.myAgent.getAID());
            msg_route.addReceiver(new AID(messageInfo.getSender().getLocalName(), AID.ISLOCALNAME));

            Tuple3<String, ArrayList<String>,Integer> nameAndRouteAndQuantity = new Tuple3<>(collectorName, routeFromTankerToInitialCollector, quantity);
            Tuple4<ArrayList<String>,ArrayList<String>,ArrayList<String>,Tuple3<String, ArrayList<String>,Integer>> a = new Tuple4<>(routeToCollector, routeToTreasure, routeToTanker, nameAndRouteAndQuantity);
            try {
                msg_route.setContentObject(a);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ((AbstractDedaleAgent)this.myAgent).sendMessage(msg_route);
            try {
                this.myAgent.doWait(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.myAgent.receive(msgTemplate,1000);
            boolean finished = true;
            for (int i = 0; i < goldTreasures.size(); ++i) {
                if (goldTreasures.get(i).getThird().get_1() > 0) {
                    finished = false;
                    break;
                }
            }
            for (int i = 0; i < diamondTreasures.size(); ++i) {
                if (diamondTreasures.get(i).getThird().get_1() > 0) {
                    finished = false;
                    break;
                }
            }
            if (finished) {
                System.out.println("I finished my task. Removing me from the map.");
                System.out.println("Total number of steps = "+STEPS);
                this.myAgent.doDelete();
            }

        }
    }

    public int onEnd()
    {
        return 1;
    }
}
