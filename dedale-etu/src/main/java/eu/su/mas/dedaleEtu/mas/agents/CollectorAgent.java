package eu.su.mas.dedaleEtu.mas.agents;

import java.util.ArrayList;
import java.util.List;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;

import eu.su.mas.dedaleEtu.mas.behaviours.*;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.FSMBehaviour;

public class CollectorAgent extends AbstractDedaleAgent {

    private String startPosition = null;
    private ArrayList<String> myRoute;
    private ArrayList<String> myRouteToTanker;
    private ArrayList<String> inverseRoute;
    private String myAction;
    private Couple<String, Integer> prevTr;
    private boolean singingStatus = true;
    private int stuck = 0;

    protected void setup(){

        super.setup();
        this.myRoute = new ArrayList<>();
        this.myRouteToTanker = new ArrayList<>();
        this.myAction = null;
        this.inverseRoute = new ArrayList<>();
        this.prevTr = new Couple<>("",-1);

        List<Behaviour> lb = new ArrayList<>();
        FSMBehaviour fsm = new FSMBehaviour();

        fsm.registerFirstState(new WaitForActionBehaviour(this,"Collector"), "singAndWait");
        fsm.registerState(new PassThroughSendBehaviour(fsm, this,"Collector"), "passSend");
        fsm.registerState(new PassThroughReceiveBehaviour(fsm, this, "Collector"), "passReceive");
        fsm.registerState(new MoveToNodeBehaviour(this, "Collector"), "moveToNode");
        fsm.registerState(new DoActionBehaviour(this,"Collector"), "doAction");

        fsm.registerTransition("singAndWait", "moveToNode", 2);
        fsm.registerTransition("singAndWait", "passReceive", 3);

        fsm.registerDefaultTransition("passSend", "passReceive");

        fsm.registerDefaultTransition("passReceive", "moveToNode");

        fsm.registerDefaultTransition("moveToNode", "moveToNode");
        fsm.registerTransition("moveToNode","doAction", 2);
        fsm.registerTransition("moveToNode","singAndWait", 3);
        fsm.registerTransition("moveToNode","passSend", 4);

        fsm.registerDefaultTransition("doAction", "moveToNode");

        lb.add(fsm);
        addBehaviour(new startMyBehaviours(this, lb));
        System.out.println("The agent "+this.getLocalName()+ " is started");
    }

    // Route
    public ArrayList<String> getMyRoute()
    {
        return myRoute;
    }

    public void setMyRoute(ArrayList<String> myRoute)
    {
        this.myRoute = myRoute;
    }

    public void addToMyRoute(String node)
    {
        this.myRoute.add(0,node);
    }


    // Inverse route
    public ArrayList<String> getInverseRoute()
    {
        return inverseRoute;
    }

    public void setInverseRoute(ArrayList<String> inverseRoute)
    {
        this.inverseRoute = inverseRoute;
    }

    public void addToInverseRoute(String node)
    {
        this.inverseRoute.add(0, node);
    }


    // Route to tanker
    public ArrayList<String> getMyRouteToTanker()
    {
        return myRouteToTanker;
    }

    public void setMyRouteToTanker(ArrayList<String> myRouteToTanker)
    {
        this.myRouteToTanker = myRouteToTanker;
    }


    // Action
    public String getMyAction()
    {
        return this.myAction;
    }

    public void setMyAction(String myAction)
    {
        this.myAction = myAction;
    }


    // Start position
    public void setStartPosition(String startPosition)
    {
        this.startPosition = startPosition;
    }

    public String getStartPosition()
    {
        return this.startPosition;
    }

    public boolean getSingingStatus() {
        return this.singingStatus;
    }

    public void setSingingStatus(boolean status) {
        this.singingStatus = status;
    }

    public int getStuck() {
        return stuck;
    }

    public void setStuck(int stuck) {
        this.stuck = stuck;
    }

    public Couple<String, Integer> getPrevTr() {
        return prevTr;
    }

    public void setPrevTr(Couple<String, Integer> prevTr) {
        this.prevTr = prevTr;
    }
}
